const withCSS = require('@zeit/next-css');
const withFonts = require('next-fonts');
const withImages = require('next-images');
const withPlugins = require("next-compose-plugins");

module.exports = withPlugins([
    withCSS,
    withFonts,
    withImages,
    {
        env: {
            host_api: 'https://super-sign-api.herokuapp.com'
            // host_api: 'http://10.5.27.215:8080'
        }
    }
]);