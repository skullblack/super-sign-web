import React, { useState, useEffect } from "react";
import Router from 'next/router';
import Link from 'next/link';
const axios = require('axios');

const Navbar = (props) => {
  const [toggle, setToggle] = useState(false);
  const [dismiss, setDismiss] = useState(false);
  const [usernameSto, setUsernamesto] = useState("");
  const [passwordSto, setPasswordsto] = useState("");
  const [firstnameSto, setFirstnamesto] = useState("");
  const [statusSto, setStatussto] = useState("");
  const [matchpass, setMatchpass] = useState(1);
  const [checklength, setChecklength] = useState(1);

  useEffect(() => {
    checkStorage();
  }, [usernameSto])

  const checkStorage = () => {
    if (localStorage.getItem("Username") && !usernameSto) {
      getStorage();
    } else if (!localStorage.getItem("Username") && usernameSto) {
      setUsernamesto("");
    }
  }

  const logout = () => {
    let keysToRemove = [
      "Id", "Firstname", "Lastname",
      "Username", "Password", "Address",
      "Tel", "Email", "Status"];

    keysToRemove.map(key => {
      localStorage.removeItem(key);
    })

    checkStorage();
    Router.push('/')
  }

  const getStorage = () => {
    setUsernamesto(localStorage.getItem("Username"));
    setPasswordsto(localStorage.getItem("Password"));
    setFirstnamesto(localStorage.getItem("Firstname"));
    setStatussto(localStorage.getItem("Status"));

  };

  const loginSubmit = (e) => {
    e.preventDefault() // หยุดการทำงาน Submit
    const Username = e.target.Username.value
    const Password = e.target.Password.value
    setDismiss(!dismiss);

    axios.get(`${process.env.host_api}/user/check`, {
      // http://ss-api.inventage.co
      headers: {
        Username: Username, //Username
        Password: Password  //Password
      }
    })
      .then(function (response) {
        // setStorage
        localStorage.setItem("Id", response.data.id);
        localStorage.setItem("Username", response.data.username);
        localStorage.setItem("Password", response.data.password);
        localStorage.setItem("Firstname", response.data.firstname);
        localStorage.setItem("Lastname", response.data.lastname);
        localStorage.setItem("Email", response.data.email);
        localStorage.setItem("Tel", response.data.tel);
        localStorage.setItem("Address", response.data.address);
        localStorage.setItem("Status", response.data.status);
      })
      .then(function () {
        getStorage();
        setTimeout(function () {
          $('#modalLRForm').modal('hide');
          setDismiss(dismiss);
          var form = document.getElementById("loginForm");
          form.reset();
        }, 1000);
      })
      .catch(function (error) {
        setDismiss(dismiss);
        console.log(error);
      });

  };

  const registerSubmit = (e) => {
    e.preventDefault()

    const Usernameregister = e.target.Usernameregister.value
    const Passwordregister = e.target.Passwordregister.value
    setDismiss(!dismiss);


    axios.put(`${process.env.host_api}/user`, {
      // http://ss-api.inventage.co

      username: Usernameregister, //Username
      password: Passwordregister  //Password

    })
      .then(function (response) {
        console.log(response.status);
        if (response.status == 205) {
          alert("มี Username นี้แล้ว")
          setDismiss(dismiss);
        } else {
          setTimeout(function () {
            $('#modalLRForm').modal('hide');
            setDismiss(dismiss);
            var form = document.getElementById("registerForm");
            form.reset();
          }, 1000);
        }

      })
      .catch(function (error) {
        setDismiss(dismiss);
        console.log(error);
      });
  }

  const matchPassword = () => {
    var Repeatpass = document.getElementById("Repeatpass");
    var Passwordregister = document.getElementById("Passwordregister");
    // console.log(Passwordregister.value.length);
    // console.log(Repeatpass.value);
    if (Passwordregister.value.length >= 10) {
      console.log("ok");
      setChecklength(2);
    } else if (Passwordregister.value.length == "") {
      setChecklength(1);
    } else {
      setChecklength(3);
    }


    if (Repeatpass.value == "" && Passwordregister.value == "") {
      setMatchpass(1)
    } else if (Repeatpass.value != Passwordregister.value) {
      setMatchpass(2)
    } else {
      setMatchpass(3)
    }

  }




  return (
    <div>
      <nav style={{ zIndex: "500" }} className={`navbar navbar-expand-lg navbar-dark fixed-top scrolling-navbar ${(() => {
        switch (props.page) {
          case "index":
            return `secondary-color`
          case "products":
            return `default-color`
          default:
            return `danger-color`
        }
      })()}`}>
        <Link href="/">
          <a className="navbar-brand" >
            <img src="../static/logo/logo-super-sign2.png" height="30" alt="mdb logo" />
          </a>
        </Link>
        <button className="navbar-toggler" onClick={() => setToggle(!toggle)} type="button" data-toggle="collapse" data-target="#navbarNavDropdown"
          aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
          <div className={toggle ? 'animated-icon1 open' : 'animated-icon1'} ><span></span><span></span><span></span></div>
        </button>

        <div className="collapse navbar-collapse" id="navbarNavDropdown">
          <ul className="navbar-nav mr-auto">
            <li className="nav-item">
              <Link href="/">
                <a className="nav-link"><i className="fas fa-home mr-1"></i>หน้าหลัก</a>
              </Link>
            </li>
            <li className="nav-item">
              <Link href="/products">
                <a className="nav-link" ><i className="fas fa-list-alt mr-1"></i>สินค้า</a>
              </Link>
            </li>
            <li className="nav-item">
              <Link href="/create">
                <a className="nav-link"><i className="fas fa-pencil-ruler mr-1"></i>ออกแบบสินค้า</a>
              </Link>
            </li>
            <li className="nav-item">
              <Link href="/payment/payment_production">
                <a className="nav-link"><i className="fas fa-clipboard-check mr-1"></i>สั่งผลิตสินค้า</a>
              </Link>
            </li>
          </ul>

          <ul className="navbar-nav ml-auto nav-flex-icons">
            {!usernameSto ? (
              <li className="nav-item ">

                <div className="dropdown">
                  <a className="nav-link p-0" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i className="fas fa-user mr-2" />เข้าสู่ระบบ
                  </a>
                  <div className="dropdown-menu dropdown-menu-right menu-right dropdown-default" >
                    <Link href="/login_register">
                      <a className="dropdown-item">
                        <i className="fas fa-sign-in-alt mr-2"></i>ลงชื่อเข้าใช้</a>
                    </Link>
                    {/* <a className="dropdown-item" data-toggle="modal" data-target="#modalLRForm" href="#">
                      <i className="fas fa-sign-in-alt mr-2"></i>ลงชื่อเข้าใช้</a> */}
                  </div>
                </div>
              </li>
            ) : (
                <li className="nav-item avatar dropdown" >
                  <a className="nav-link p-0" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i className="fas fa-user-cog">{firstnameSto ? ` ยินดีต้อนรับ ${firstnameSto}` : ""}</i>
                  </a>
                  <div className="dropdown-menu dropdown-menu-lg-right dropdown-secondary" >
                    <Link href="/profile">
                      <a className="dropdown-item"><i className="fas fa-user-alt pr-2"></i>โปรไฟล์</a>
                    </Link>
                    <Link href="/payment/orders">
                      <a className="dropdown-item"><i className="fas fa-list-alt pr-2"></i>สินค้าสั่งซื้อ</a>
                    </Link>
                    <Link href="/payment/production_order">
                      <a className="dropdown-item"><i className="fas fa-clipboard-check pr-2"></i>สินค้าสั่งผลิต</a>
                    </Link>
                    {statusSto == "admin" ? (
                      <div>
                        <div className="dropdown-divider" />
                        <Link href="/management">
                          <a className="dropdown-item "><i className="fas fa-wrench pr-2"></i>ระบบจัดการ</a>
                        </Link>
                      </div>
                    ) : ""}
                    <div className="dropdown-divider" />
                    <a className="dropdown-item font-weight-bold" onClick={logout}><i className="fas fa-sign-out-alt pr-1"></i>ลงชื่อออก</a>

                  </div>
                </li>
              )}
          </ul>
        </div>
      </nav>





      <div className="modal fade " id="modalLRForm" tabIndex={-1} role="dialog" data-keyboard="false" data-backdrop="static" aria-labelledby="myModalLabel" aria-hidden="true">
        <div className="modal-dialog cascading-modal" role="document">
          <div className="modal-content">
            <div className="modal-c-tabs">
              <ul className="nav nav-tabs md-tabs tabs-2 teal lighten-5" role="tablist">
                <li className="nav-item">
                  <a className={!dismiss ? "nav-link active" : "nav-link active disabled"} data-toggle="tab" href="#loginForm" role="tab"><i className="fas fa-user mr-1" />
                    Login</a>
                </li>
                <li className="nav-item">
                  <a className={!dismiss ? "nav-link" : "nav-link disabled"} data-toggle="tab" href="#registerForm" role="tab"><i className="fas fa-user-plus mr-1" />
                    Register</a>
                </li>
              </ul>
              <div className="tab-content">

                <form action="#" onSubmit={(e) => loginSubmit(e)} className="tab-pane fade in show active" id="loginForm" role="tabpanel">
                  <div className="modal-body mb-1">
                    <div className="md-form form-sm mb-5">
                      <i className="fas fa-user-alt prefix" />
                      <input type="text" name="Username" id="Username" className="form-control form-control-sm validate" disabled={dismiss} required />
                      <label data-error="wrong" data-success="right" htmlFor="Username">Username</label>
                    </div>
                    <div className="md-form form-sm mb-4">
                      <i className="fas fa-lock prefix" />
                      <input type="password" name="Password" id="Password" className="form-control form-control-sm validate" disabled={dismiss} required />
                      <label data-error="wrong" data-success="right" htmlFor="Password">Your password</label>
                    </div>
                    <div className="text-center mt-2">
                      <button className="btn btn-info" disabled={dismiss}>
                        <span className={dismiss ? 'spinner-grow spinner-grow-sm' : ''} role="status" aria-hidden="true"></span>
                        {dismiss ? 'Loading...' : 'Log in'}
                      </button>
                    </div>
                  </div>
                  <div className="modal-footer">
                    <div className="options text-center text-md-right mt-1">
                      <p>Not a member? <a href="#" className={!dismiss ? "blue-text" : "deep-orange-text disabled"}>Sign Up</a></p>
                      <p>Forgot <a href="#" className={!dismiss ? "blue-text" : "deep-orange-text disabled"}>Password?</a></p>
                    </div>
                    <button type="button" className="btn btn-outline-info waves-effect ml-auto" data-dismiss="modal" disabled={dismiss}>Close</button>
                  </div>
                </form>

                <form action="#" onSubmit={(e) => registerSubmit(e)} className="tab-pane fade" id="registerForm" role="tabpanel">
                  <div className="modal-body">
                    <div className="md-form form-sm mb-5">
                      <i className="fas fa-user-alt prefix" />
                      <input type="text" name="Usernameregister" id="Usernameregister" className="form-control form-control-sm validate" disabled={dismiss} required />
                      <label data-error="wrong" data-success="right" htmlFor="Usernameregister">Username</label>
                    </div>
                    <div className="md-form form-sm mb-5">
                      <i className="fas fa-lock prefix" />
                      <input type="password" name="Passwordregister" id="Passwordregister" onChange={matchPassword}
                        className={checklength == 2 ? "form-control form-control-sm valid" : checklength == 3 ? "form-control form-control-sm  invalid" : "form-control form-control-sm"} disabled={dismiss} required />
                      {/* <label data-error="wrong" data-success="right" htmlFor="Passwordregister">Your password</label> */}
                      {
                        checklength == 2 ? <label htmlFor="Passwordregister">Your password</label>
                          : <label data-error="รหัส10ตัวขึ้นไป" htmlFor="Passwordregister">Your password</label>
                      }
                    </div>
                    {/* {checklength ?  */}
                    <div className="md-form form-sm mb-4">
                      <i className="fas fa-lock prefix" />
                      <input type="password" name="Repeatpass" id="Repeatpass" onChange={matchPassword}
                        className={matchpass == 2 ? "form-control form-control-sm  invalid" : matchpass == 3 ? "form-control form-control-sm valid" : "form-control form-control-sm"} disabled={dismiss} required />
                      {matchpass == 3 ? (
                        <label htmlFor="Repeatpass">Repeat password</label>
                      ) : (
                          <label data-error="รหัสไม่เหมือนกัน" htmlFor="Repeatpass">Repeat password</label>
                        )}
                    </div>
                    {/* :""} */}
                  </div>
                  <div className="text-center form-sm mt-2">
                    <button className="btn btn-info" disabled={matchpass != 3 ? "disabled" : ""} >
                      <span className={dismiss ? 'spinner-grow spinner-grow-sm' : ''} role="status" aria-hidden="true"></span>
                      {dismiss ? 'Loading...' : 'Sign up'} </button>
                  </div>
                  <div className="modal-footer">
                    <div className="options text-right">
                      <p className="pt-1">Already have an account?
                      <a className={!dismiss ? "blue-text" : "deep-orange-text disabled"} >Sign In</a></p>
                    </div>
                    <button type="button" className="btn btn-outline-info waves-effect ml-auto" data-dismiss="modal" disabled={dismiss} >Close</button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>




      <style jsx>{`
      @media screen and (max-width: 991.98px) { 
        .menu-right {  
          left:0;
        }
       }

      .cursor-pointer{
        cursor: pointer;
      }
            
      .animated-icon1{
        width: 30px;
        height: 20px;
        position: relative;
        margin: 0px;
        -webkit-transform: rotate(0deg);
        -moz-transform: rotate(0deg);
        -o-transform: rotate(0deg);
        transform: rotate(0deg);
        -webkit-transition: .5s ease-in-out;
        -moz-transition: .5s ease-in-out;
        -o-transition: .5s ease-in-out;
        transition: .5s ease-in-out;
        cursor: pointer;
      }

      .animated-icon1 span{
        display: block;
        position: absolute;
        height: 3px;
        width: 90%;
        border-radius: 9px;
        opacity: 1;
        left: 0;
        -webkit-transform: rotate(0deg);
        -moz-transform: rotate(0deg);
        -o-transform: rotate(0deg);
        transform: rotate(0deg);
        -webkit-transition: .25s ease-in-out;
        -moz-transition: .25s ease-in-out;
        -o-transition: .25s ease-in-out;
        transition: .25s ease-in-out;
      }

      .animated-icon1 span {
        background: #fafafa;
      }
      .animated-icon1 span:nth-child(1) {
        top: 0px;
      }

      .animated-icon1 span:nth-child(2) {
        top: 9px;
      }

      .animated-icon1 span:nth-child(3) {
        top: 18px;
      }

      .animated-icon1.open span:nth-child(1) {
        top: 11px;
        -webkit-transform: rotate(135deg);
        -moz-transform: rotate(135deg);
        -o-transform: rotate(135deg);
        transform: rotate(135deg);
      }

      .animated-icon1.open span:nth-child(2) {
        opacity: 0;
        left: -60px;
      }

      .animated-icon1.open span:nth-child(3) {
        top: 11px;
        -webkit-transform: rotate(-135deg);
        -moz-transform: rotate(-135deg);
        -o-transform: rotate(-135deg);
        transform: rotate(-135deg);
      }
    `}
      </style>
    </div>
  );
};

export default Navbar;
