import React, { useState, useEffect } from "react";
import { useRouter } from 'next/router';
import Link from 'next/link';
import Navbar from './Navbar';
import Footer from './footer';
import LoadingOverlay from 'react-loading-overlay';


const Layout = (props) => {
  const router = useRouter()
  const [countProduct, setCountproduct] = useState('');

  const count = () => {
    var stoCartcheck = JSON.parse(localStorage.getItem("cart"));
    if (stoCartcheck && stoCartcheck.length) {

      var countOrder = stoCartcheck.map(o => o.count).reduce((a, c) => { return parseInt(a) + parseInt(c) });
      setCountproduct(countOrder);
    }
  }
  useEffect(() => {

  }, [router.pathname])

  useEffect(() => {
    count();
  }, [props.count])

  return (


    <div>
      <Navbar
        page={props.page}
      />
      {router.pathname == "/"
        || router.pathname == "/products" || router.pathname == "/single_product" ? (
          <div className="cart-style">
            <Link href="/payment/carts">
              <a className="cart" style={{ right: "2vh" }}><span className={countProduct ? 'counter' : ''}>{countProduct}</span></a>
            </Link>
          </div>
        ) : ''}
      {props.children}
      {/* <Footer /> */}
      <style jsx>{`
        
                .cart-style{
                    position:fixed;
                    top: 45px;
                    right: 0;
                    z-index: 1;
                }
                
                a.cart {
                    width: 72px;
                    height: 72px;
                    -moz-border-radius: 36px;
                    -webkit-border-radius: 36px;
                    border-radius: 36px;
                    font-size: 18px;
                    text-align: center;
                    color: #616161;
                    text-decoration: none;
                    position: absolute;
                    right: 24px;
                    top: 36px;
                    display: block;
                    background: #535558 url(http://images.vfl.ru/ii/1484930184/14435803/15757225.png
                    ) center no-repeat;
                    background-size: 36px;
                    box-shadow: 0 3px 6px rgba(97, 97, 97 0.16), 0 3px 6px rgba(97, 97, 97 0.23);
                    -moz-box-shadow: 0 3px 6px rgba(97, 97, 97 0.16), 0 3px 6px rgba(97, 97, 97 0.23);
                    -webkit-box-shadow: 0 3px 6px rgba(97, 97, 97 0.16), 0 3px 6px rgba(97, 97, 97 0.23);
                }

                a.cart > span {
                    width: 24px;
                    height: 24px;
                    font-size: 16px;
                    color: #fff;
                    line-height: 24px;
                    position: absolute;
                    -moz-border-radius: 12px;
                    -webkit-border-radius: 12px;
                    border-radius: 12px;
                    display: block;
                    transform: scale(0);
                    -o-transform: scale(0);
                    -ms-transform: scale(0);
                    -moz-transform: scale(0);
                    -webkit-transform: scale(0);
                    top: 0;
                    right: 0;
                    bottom: 0;
                    left: 0;
                    margin: auto;
                    background: #ffab00;
                    box-shadow: 0 3px 6px rgba(255, 171, 0, 0.16), 0 3px 6px rgba(255, 171, 0, 0.23);
                    -moz-box-shadow: 0 3px 6px rgba(255, 171, 0, 0.16), 0 3px 6px rgba(255, 171, 0, 0.23);
                    -webkit-box-shadow: 0 3px 6px rgba(255, 171, 0, 0.16), 0 3px 6px rgba(255, 171, 0, 0.23);
                }

                a.cart > span.counter {
                    left: 48px;
                    bottom: 48px;
                    -webkit-transition: .2s linear;
                    -moz-transition: .2s linear;
                    -ms-transition: .2s linear;
                    -o-transition: .2s linear;
                    transition: .2s linear;
                    transform: scale(1);
                    -o-transform: scale(1);
                    -ms-transform: scale(1);
                    -moz-transform: scale(1);
                    -webkit-transform: scale(1);
                }
            `}</style>
      <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
      <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.4/umd/popper.min.js"></script>
      <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/js/bootstrap.min.js"></script>
      <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.10.1/js/mdb.min.js"></script>
    </div>
  );
}


export default Layout;
