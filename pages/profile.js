import React, { useState, useEffect } from "react";
import Layout from '../components/Layout';
import { useRouter } from 'next/router';
import axios from "axios";

const profile = () => {
  const router = useRouter();
  const [datausersprofile, setDatauserprofile] = useState([]);
  useEffect(() => {
    getStoprofile();
  }, [])
  const getStoprofile = () => {
      if (localStorage.getItem("Id")) {
        document.getElementById("editId").value = localStorage.getItem("Id");
      }
      if (localStorage.getItem("Username")) {
        document.getElementById("usernameprofile").value = localStorage.getItem("Username");
        document.getElementById("editUsername").value = localStorage.getItem("Username");
        document.getElementById("editUsername").nextElementSibling.classList.add("active");
      }
      if (localStorage.getItem("Password")) {
        document.getElementById("editPassword").value = localStorage.getItem("Password");
        document.getElementById("editPassword").nextElementSibling.classList.add("active");
      }
      if (localStorage.getItem("Firstname")) {
        document.getElementById("firstnameprofile").value = localStorage.getItem("Firstname");
        document.getElementById("editFirstname").value = localStorage.getItem("Firstname");
        document.getElementById("editFirstname").nextElementSibling.classList.add("active");
      }
      if (localStorage.getItem("Lastname")) {
        document.getElementById("lastnameprofile").value = localStorage.getItem("Lastname");
        document.getElementById("editLastname").value = localStorage.getItem("Lastname");
        document.getElementById("editLastname").nextElementSibling.classList.add("active");
      }
      if (localStorage.getItem("Email")) {
        document.getElementById("emailprofile").value = localStorage.getItem("Email");
        document.getElementById("editEmail").value = localStorage.getItem("Email");
        document.getElementById("editEmail").nextElementSibling.classList.add("active");
      }
      if (localStorage.getItem("Tel")) {
        document.getElementById("telprofile").value = localStorage.getItem("Tel");
        document.getElementById("editTel").value = localStorage.getItem("Tel");
        document.getElementById("editTel").nextElementSibling.classList.add("active");
      }
      if (localStorage.getItem("Address")) {
        document.getElementById("addressprofile").value = localStorage.getItem("Address");
        document.getElementById("editAddress").value = localStorage.getItem("Address");
        document.getElementById("editAddress").nextElementSibling.classList.add("active");
      }
   



  }
  const logout = () => {
    let keysToRemove = [
      "Id", "Firstname","Lastname", 
      "Username","Password", "Address",
      "Tel", "Email", "Status"];

      keysToRemove.map(key => {
        localStorage.removeItem(key);
      }) 

    router.push('/');
  }

  const saveEdituser = (e) => {
    e.preventDefault();
    var obj = e;
    axios.post(`${process.env.host_api}/user`, {
      id: e.target.editId.value,
      username: e.target.editUsername.value,
      password: e.target.editPassword.value,
      firstname: e.target.editFirstname.value,
      lastname: e.target.editLastname.value,
      email: e.target.editEmail.value,
      tel: e.target.editTel.value,
      address: e.target.editAddress.value

    })
      .then(response => {
        console.log(response.data);
        $('#modaleditUser').modal('hide');
        alert("กรุณา login ใหม่")
        logout();
        

      })
      .catch(function (error) {
        console.log(error);

      });

  }



  return (
    <Layout>
      {/* modaledituser */}

      <div className="top" />
      <div className="modal fade" id="modaleditUser" tabIndex={-1} role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div className="modal-dialog modal-lg" role="document">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title" id="exampleModalLongTitle">แก้ไข โปรไฟล์</h5>
              <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
              </button>
            </div>
            <div className="modal-body">
              <form onSubmit={((e) => saveEdituser(e))}>
                <input type="hidden" className="form-control" id="editId" />
                <div className="form-row">
                  <div className="col-md-6">
                    <div className="md-form form-group">
                      <input type="text" className="form-control" id="editUsername" required />
                      <label htmlFor="editUsername">รหัสผู้ใช้</label>
                    </div>
                  </div>
                  <div className="col-md-6">
                    <div className="md-form form-group">
                      <input type="password" className="form-control" id="editPassword" required />
                      <label htmlFor="editPassword">รหัสผ่าน</label>
                    </div>
                  </div>
                </div>
                <div className="form-row">
                  <div className="col-md-6">
                    <div className="md-form form-group">
                      <input type="text" className="form-control" id="editFirstname" />
                      <label htmlFor="editFirstname">ชื่อ</label>
                    </div>
                  </div>
                  <div className="col-md-6">
                    <div className="md-form form-group">
                      <input type="text" className="form-control" id="editLastname" />
                      <label htmlFor="editLastname">สกุล</label>
                    </div>
                  </div>
                </div>
                <div className="form-row">
                  <div className="col-md-6">
                    <div className="md-form form-group">
                      <input type="email" className="form-control" id="editEmail" />
                      <label htmlFor="editEmail">อีเมลล์</label>
                    </div>
                  </div>
                  <div className="col-md-6">
                    <div className="md-form form-group">
                      <input type="text" className="form-control" id="editTel" />
                      <label htmlFor="editTel">เบอร์โทร</label>
                    </div>
                  </div>
                </div>
                <div className="row">
                  <div className="col-md-12">
                    <div className="md-form form-group">
                      <textarea id="editAddress" className="form-control md-textarea " />
                      <label htmlFor="editAddress">ที่อยู่</label>
                    </div>
                  </div>
                </div>
                <div className="modal-footer">
                  <button type="button" className="btn btn-outline-secondary  waves-effect mr-2" data-dismiss="modal">ยกเลิก</button>
                  <button className="btn btn-outline-success  waves-effect">บันทึก</button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>

      <div className=" size-margin my-5 p-5 grey lighten-5">
        <form className="form" action="##" method="post" id="registrationForm">
          <div className="row">
            <div className="col-xl-12 my-2">
              <h2 className="text-center font-weight-bold mb-4 pb-2">โปรไฟล์</h2>
            </div>
          </div>
          <div className="form-group row">
            <label htmlFor="searchUsername" className="col-sm-3 col-form-label font-weight-normal">รหัสผู้ใช้</label>
            <div className="col-sm-9">
              <input type="text" id="usernameprofile" className="form-control border-0" disabled />
            </div>
          </div>
          <div className="form-group row">
            <label htmlFor="searchFirstname" className="col-sm-3 col-form-label font-weight-normal">ชื่อ</label>
            <div className="col-sm-9">
              <input type="text" id="firstnameprofile" className="form-control border-0" disabled />
            </div>
          </div>
          <div className="form-group row">
            <label htmlFor="searchLastname" className="col-sm-3 col-form-label font-weight-normal">สกุล</label>
            <div className="col-sm-9">
              <input type="text" id="lastnameprofile" className="form-control border-0" disabled />
            </div>
          </div>
          <div className="form-group row">
            <label htmlFor="searchEmail" className="col-sm-3 col-form-label font-weight-normal">อีเมลล์</label>
            <div className="col-sm-9">
              <input type="text" id="emailprofile" className="form-control border-0" disabled />
            </div>
          </div>
          <div className="form-group row">
            <label htmlFor="searchTel" className="col-sm-3 col-form-label font-weight-normal">เบอร์โทร</label>
            <div className="col-sm-9">
              <input type="text" id="telprofile" className="form-control border-0" disabled />
            </div>
          </div>
          <div className="form-group row">
            <label htmlFor="searchAddress" className="col-sm-3 col-form-label font-weight-normal">ที่อยู่</label>
            <div className="col-sm-9">
              <textarea id="addressprofile" className="form-control md-textarea noresize" disabled />
            </div>
          </div>
          <div className="row">
            <div className="col-xl-12 mt-2">
              <button type="button" className="btn btn-warning float-right mr-3" data-toggle="modal" data-target="#modaleditUser"><i className="fas fa-edit mr-2"></i>แก้ไข</button>
            </div>
          </div>
        </form>
      </div>


      <style jsx>{`
    .top{
      margin-top: 100px;
    }
    .noresize {
      resize: none; 
    }
    .size-margin{
      margin-left: 20%;
      margin-right: 20%;
    }
    @media screen and (max-width: 992px) { 
        .size-margin{
        margin-left: 10%;
        margin-right: 10%;
      }
    }
    @media screen and (max-width: 600px) { 
        .size-margin{
        margin-left: 5%;
        margin-right: 5%;
      }
    }
    
    `}</style>
    </Layout>
  );
}

export default profile;
