import React, { useState, useEffect } from 'react';
import Layout from '../components/Layout';
import { useRouter } from 'next/router';
import Pagination from "react-js-pagination";
import Head from "next/head";
const axios = require('axios');

const products = () => {
    const router = useRouter();
    const [rangStart, setRangstart] = useState();
    const [rangEnd, setRangend] = useState();
    const [dataProduct, setDataproduct] = useState([]);
    const [dataProductall, setDataproductall] = useState([]);
    const [typeproduct, setTypeproduct] = useState([]);
    const [countProduct, setCountproduct] = useState("");
    const [toggle, setToggle] = useState(false);
    const [showOnload, setShowonload] = useState(false);

    const [activePage, setActivePage] = useState(1);
    const [productLength, setProductLength] = useState("");
    const [productWithPage, setProductWithPage] = useState([]);
    const itemsCountPerPage = 12;
    const pageRangeDisplayed = 10;



    const addCart = (e) => {
        var productid = e.target.productId.value;
        var productimg = e.target.productImg.value;
        var productname = e.target.productName.value;
        var productdetails = e.target.productDetails.value;
        var productprice = e.target.productPrice.value;
        e.preventDefault();
        $(e.target).slideUp(300).slideDown(300);

        if (!localStorage.getItem("cart")) {
            var carts = [];
            var setitem = {
                id: productid,
                img: productimg,
                name: productname,
                details: productdetails,
                price: productprice,
                count: 1
            };
            carts.push(setitem);
            localStorage.setItem("cart", JSON.stringify(carts));
        } else {
            var stoCart = JSON.parse(localStorage.getItem("cart"));
            var objIndex = stoCart.findIndex((obj => obj.id == productid));

            if (objIndex >= 0) {

                stoCart[objIndex].count += 1;
                localStorage.setItem("cart", JSON.stringify(stoCart));
            } else {
                var setitem = {
                    id: productid,
                    img: productimg,
                    name: productname,
                    details: productdetails,
                    price: productprice,
                    count: 1
                };
                stoCart.push(setitem);
                localStorage.setItem("cart", JSON.stringify(stoCart));

            }
        }
        var stoCartcheck = JSON.parse(localStorage.getItem("cart"));
        let sum = stoCartcheck.map(o => o.count).reduce((a, c) => { return a + c });
        setCountproduct(sum);
    }
    useEffect(() => {
        getProduct();
        getTypeproduct();
    }, [])
    useEffect(() => {
        range();
    }, [toggle,dataProduct])
    useEffect(() => {
        var end = Math.max.apply(Math, dataProductall.map(o => o.price));
        setRangend(end);
        setRangstart(end);
    }, [dataProductall])

    const getProduct = () => {
        axios.get(`${process.env.host_api}/storage`, {
            // http://ss-api.inventage.co

        })
            .then(response => {
                setDataproductall(response.data);
                setShowonload(!showOnload);
                setProductLength(response.data.length);
                setProductPage(activePage, response.data)
            })
            .catch(function (error) {
                console.log(error);

            });
    }
    const getTypeproduct = () => {
        axios.get(`${process.env.host_api}/storage/type`, {
            // http://ss-api.inventage.co

        })
            .then(response => {

                setTypeproduct(response.data);


            })
            .catch(function (error) {
                console.log(error);

            });
    }
    const single_product = (id) => {
        router.push({
            pathname: '/single_product',
            query: {
                product_id: id
            },
        })
    }

    const range = () => {

        var start = document.getElementById('start').value;
        var end = document.getElementById('end').value;

        setRangstart(end);
        document.getElementById('startprice').innerHTML = Number(start).toLocaleString();
        document.getElementById('endprice').innerHTML = Number(end).toLocaleString();

        if (toggle) {
            var setend = dataProduct.filter(function (obj) {
                return obj.price >= start && obj.price <= end && document.getElementById(obj.type).checked;
            });
            setProductWithPage(setend);
        } else {
            setProductWithPage(dataProduct);
        }


    }

    const handlePageChange = (pageNumber) => {
        var result = dataProductall.slice((pageNumber - 1) * itemsCountPerPage, itemsCountPerPage * pageNumber);
        setActivePage(pageNumber);
        setProductWithPage(result);
        setDataproduct(result);

    }

    const setProductPage = (pageNumber, data) => {
        var result = data.slice((pageNumber - 1) * itemsCountPerPage, itemsCountPerPage * pageNumber);

        setDataproduct(result);
        setProductWithPage(result);
        setActivePage(pageNumber)
    }

    return (
        <Layout
            page="products"
            count={countProduct}
        >
            <Head>
                <title>Product</title>
            </Head>
            <div className="container-fluid" >
                <div style={{ height: "90px" }}></div>
                <div className=" text-center display-4 mt-4">
                    <img src="../static/logo/product-logo.png" height="70" alt="mdb logo" />
                </div>
                <div className="row my-5 ">
                    <div className="col col-sm-12 col-md-4 col-lg-3 col-xl-2 mb-4">
                        <div className="card">
                            <div className="card-body">
                                <form onInput={range}>
                                    <input type="range" className="custom-range rang" id="start" max={rangStart} defaultValue="0" />
                                    <div>ราคาต่ำสุด: <span id="startprice" className="text-warning">0</span><span> ฿</span></div>
                                    <input type="range" className="custom-range rang" id="end" max={dataProductall.length ? Math.max.apply(Math, dataProductall.map(o => o.price)) : "0"} defaultValue={dataProductall.length ? Math.max.apply(Math, dataProductall.map(o => o.price)) : "0"} />
                                    <div>ราคาสูงสุด: <span id="endprice" className="text-warning">{dataProductall.length ? Math.max.apply(Math, dataProductall.map(o => o.price)) : "0"}</span><span> ฿</span></div>
                                    <hr />
                                    {typeproduct.map((type, i) => (
                                        <div key={i} className="custom-control custom-switch">
                                            <input type="checkbox" className="custom-control-input" id={type.id} defaultChecked />
                                            <label className="custom-control-label" htmlFor={type.id}>{type.type_pd}</label>
                                        </div>
                                    ))}
                                    <hr />
                                    <button onClick={(() => setToggle(!toggle))} type="button" className={`btn btn-block ${toggle ? "btn-outline-success" : "btn-outline-info"} pt-2  pb-0 my-2`}>
                                        <h6>{toggle ? <i className="fas fa-check mr-2" /> : ""}ช่วยตัดสินใจ</h6>
                                    </button>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div className="col col-sm-12  col-md-8 col-lg-9 col-xl-10">
                        <div className="card ">
                            <div className="card-body">
                                <div className="row justify-content-around">
                                    {productWithPage.length ? productWithPage.map((prod, i) => (
                                        <div key={i} className=" col-md-10 col-lg-5 col-xl-3 mb-4">
                                            <form onSubmit={((e) => addCart(e))}>
                                                <input type="hidden" name="productId" value={prod.id} />
                                                <div className="card m-auto">
                                                    <div className="card-img-top img-border-radius p-2 img-style" style={{
                                                        backgroundImage: 'url(' + (`${prod.picture}`) + ')'
                                                    }}>
                                                        <input type="hidden" name="productImg" value={prod.picture} />
                                                        <img className="card-img-top img-border-radius p-2" src={prod.picture} alt="Card image cap" />
                                                    </div>
                                                    <div className="card-body">
                                                        <h5 className="card-title text-truncate">
                                                            <input type="hidden" name="productName" value={prod.name} />
                                                            <a onClick={(() => single_product(prod.id))}>{prod.name}</a>
                                                        </h5>
                                                        <p className="card-text p-0 crop-text-2">
                                                            <input type="hidden" name="productDetails" value={prod.details} />
                                                            {prod.details}
                                                        </p>
                                                    </div>
                                                    <div className="card-footer">
                                                        <span className="float-left">
                                                            <input type="hidden" name="productPrice" value={prod.price} />
                                                            {Number(prod.price).toLocaleString()+".00"}฿
                                                    </span>
                                                        <span className="float-right">
                                                            <button style={{ background: "none", outline: "none", border: "none" }} className="cart-style mr-2"><i className="fas fa-shopping-cart"></i></button>

                                                        </span>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    ))

                                        : showOnload ?
                                            <div className="text-center">
                                                NOT DATA
                                            </div>
                                            :
                                            <div className="spinner-border text-primary" role="status">
                                                <span className="sr-only">Loading...</span>
                                            </div>
                                    }
                                </div>
                                {productLength ?
                                    <div>
                                        <hr />
                                        <div className="d-flex justify-content-center">
                                            <Pagination
                                                activePage={activePage}
                                                innerClass="pagination pg-blue"
                                                itemClass="page-item"
                                                linkClass="page-link"
                                                itemsCountPerPage={itemsCountPerPage}
                                                totalItemsCount={productLength}
                                                pageRangeDisplayed={pageRangeDisplayed}
                                                onChange={handlePageChange}
                                            />
                                        </div>
                                    </div>
                                    : ""}
                            </div>

                        </div>
                    </div>
                </div>
                {/* <div className="card my-5 py-4 px-0 px-sm-4 px-md-4 px-lg-4 px-xl-4 mx-0 mx-sm-5 mx-md-5 mx-lg-5 mx-xl-5">
                    <div className="card-body">
                        <div className="row justify-content-around">
                            {dataProduct.length ? dataProduct.map((prod, i) => (
                                <div key={i} className="col-md-5 col-lg-5 col-xl-3 mb-4">
                                    <form onSubmit={((e) => addCart(e))}>
                                        <input type="hidden" id="productId" value={prod.id} />
                                        <div className="card m-auto">
                                            <div className="card-img-top img-border-radius p-2 img-style" style={{
                                                backgroundImage: 'url(' + (`${prod.picture}`) + ')'
                                            }}>
                                                <input type="hidden" id="productImg" value={prod.picture} />
                                                <img className="card-img-top img-border-radius p-2"  src={prod.picture} alt="Card image cap" />
                                            </div>
                                            <div className="card-body">
                                                <h5 className="card-title">
                                                    <input type="hidden" id="productName" value={prod.name} />
                                                    <a onClick={(()=> single_product(prod.id))}>{prod.name}</a>
                                                </h5>
                                                <p className="card-text p-0 crop-text-2">
                                                    <input type="hidden" id="productDetails" value={prod.details} />
                                                    {prod.details}
                                                </p>
                                            </div>
                                            <div className="card-footer">
                                                <span className="float-left">
                                                    <input type="hidden" id="productPrice" value={prod.price} />
                                                    {prod.price}฿
                                                    </span>
                                                <span className="float-right">
                                                    <button  style={{ background: "none", outline: "none", border: "none" }} className="cart-style mr-2"><i className="fas fa-shopping-cart"></i></button>
                                                   
                                                </span>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            )) : (
                                    <div className="spinner-border text-primary" role="status">
                                        <span className="sr-only">Loading...</span>
                                    </div>
                                )}
                        </div>
                    </div>
                    
                </div> */}
            </div>
            <style jsx>{`
                
                .img-block {
                    display: block;
                    margin-left: auto;
                    margin-right: auto;
                }
                .img-style{
                    background-position: center;
                    background-repeat: no-repeat;
                    height: 0;
                    width:0;
                    background-size: contain;
                    padding: 30% 50% !important
                }
                .cart-style:hover{
                    color: green;
                }
                .crop-text-2 {
                    -webkit-line-clamp: 2;
                    overflow : hidden;
                    text-overflow: ellipsis;
                    display: -webkit-box;
                    -webkit-box-orient: vertical;
                }
                
                `}</style>
        </Layout >
    );
}

export default products;
