import React, { useState, useEffect } from 'react';
import Layout from '../components/Layout';
import Link from 'next/link';
import { fabric } from 'fabric';

const create = () => {
  useEffect(() => {
    //=================================================== create fabric.js =======================================================
    var canvas = new fabric.Canvas('canvas');
    //============================================================================================================================
    //====================================================== add image ===========================================================
    $(document).on('change', '#file', function (e) {
      var file = e.target.files[0];
      e.target.value = "";
      var reader = new FileReader();
      reader.onload = function (f) {
        var data = f.target.result;
        fabric.Image.fromURL(data, function (img) {
          console.log(img);

          var oImg = img.set().scale(0.2);
          canvas.add(oImg).renderAll();
        }, { crossOrigin: 'anonymous' });
      };
      reader.readAsDataURL(file);

    });

    $(document).on('click', '.img-make', function (e) {

      fabric.Image.fromURL(e.target.src, function (myImg) {
        var img1 = myImg.set({
          left: 230,
          top: 130,
        }).scale(0.2);
        canvas.add(img1).renderAll();
      }, { crossOrigin: 'anonymous' });

    });
    //========ยังไม่เสร็จ
    $(document).on('change', '.img_color', function () {
      var activeObject = canvas.getActiveObject();
      // console.log(activeObject);
      // activeObject.filters.push({ color: this.color });
      // activeObject.applyFilters();
      // canvas.renderAll();
      // applyFilter(12, new f.Tint({
      //   color: "red",
      //   opacity: 0.5
      // }));
      // var filter = new fabric.Image.filters.Tint({
      //   color: "#f000"
      // })
      // activeObject.filters.push(filter);
      // activeObject.applyFilters();
      // canvas.renderAll();
    });


    //=============================================================================================================================

    //============================================================== text tool=====================================================
    $(document).on('click', '.add_text', function () {
      canvas.add(new fabric.Textbox('Text', {
        left: 350,
        top: 200,
      }));
      canvas.renderAll();
    });

    $(document).on('change', '.text_color', function () {

      if (canvas.getActiveObject()) {
        
        canvas.getActiveObject().setColor(this.value); //set({fill: this.value})
        canvas.renderAll();
      }
    });

    $(document).on('change', '.text_bg', function () {
      if (canvas.getActiveObject()) {

        canvas.getActiveObject().set("backgroundColor", this.value);
        canvas.renderAll();
      }
    });
    //============================================================================================================================

    //======================================================== clear all canvas ==================================================
    $(document).on('click', '.clear_canvas', function () {

      if (confirm("คุณต้องการ ลบ design ทั้งหมดใช่หรือไม่")) {
        canvas.clear();

      }
    });
    //============================================================================================================================
    //================================================ clear object on active in canvas ==========================================
    $(document).on('click', '.clear_obj', function () {

      canvas.remove(canvas.getActiveObject());
    });
    //============================================================================================================================

    $(document).on('click', '.obj_up', function () {
      var selectedObject = canvas.getActiveObject();
      if (selectedObject)
        canvas.bringToFront(selectedObject);
      canvas.renderAll();
    });
    $(document).on('click', '.obj_down', function () {
      var selectedObject = canvas.getActiveObject();
      if (selectedObject)
        canvas.sendToBack(selectedObject);
      canvas.renderAll();
    });

    canvas.on('mouse:down', function (e) {

      if (e.target) {
        if ((e.target.get('type')) === "textbox") {
          var x = document.getElementById("text-tab").classList.contains("active");
          if (!x) {
            document.getElementById("images-tab").classList.remove("active");
            document.getElementById("images").classList.remove("active", "show");
            document.getElementById("text-tab").classList.add("active");
            document.getElementById("text").classList.add("active", "show");
          }
          document.getElementById("font_area").style.display = "block";
          // document.getElementById("img_design_area").style.display = "none";
        }
        // ไม่ใช้แล้ว
        //  else if ((e.target.get('type')) === "image") {
        //   document.getElementById("img_area").style.display = "none";
        //   document.getElementById("img_design_area").style.display = "block";
        //   document.getElementById("font_area").style.display = "none";
        //   var x = document.getElementById("images-tab").classList.contains("active");
        //   if (!x) {
        //     document.getElementById("text-tab").classList.remove("active");
        //     document.getElementById("text").classList.remove("active", "show");
        //     document.getElementById("contact-tab").classList.remove("active");
        //     document.getElementById("contact").classList.remove("active", "show");
        //     document.getElementById("images-tab").classList.add("active");
        //     document.getElementById("images").classList.add("active", "show");
        //   }

        // }
      } else {
        // document.getElementById("img_area").style.display = "block";
        // document.getElementById("img_design_area").style.display = "none";
        document.getElementById("font_area").style.display = "none";
      }
    });



    $(document).on('click', 'input:radio', function () {
      var obj = canvas.getActiveObject();
      if (obj) {
        obj.set("fontFamily", this.value);
        canvas.requestRenderAll();

      }

    });


    setvaluelableFont();
  }, [])



  const setvaluelableFont = () => {

    for (var i = 1; i <= 14; i++) {
      var value = document.getElementById(`value_${i}`).value;
      document.getElementById(`valueSet_${i}`).style.fontFamily = value;

    }

  }
  function DownloadCanvasAsImage() {
    let downloadLink = document.createElement('a');
    downloadLink.setAttribute('download', 'CanvasAsImage.png');
    let CanvasAsImage = document.getElementById('canvas');
    let dataURL = CanvasAsImage.toDataURL('image/png');
    let url = dataURL.replace(/^data:image\/png/, 'data:application/octet-stream');
    downloadLink.setAttribute('href', url);
    downloadLink.click();
  }


  return (
    <Layout>


      <div className="container-fluid ">
        <div className="card" style={{ width: "100%", marginTop: "10vh" }}>
          <div className="card-body">
            <div className="row">
              <div className="col">
                <canvas style={{ border: "1px solid " }} id="canvas" width="848" height="450"></canvas>
              </div>
              <div className="col">
                <div className="btn-group btn-block" role="group" aria-label="Basic example">
                  <button type="button" className="btn btn-outline-info btn-rounded waves-effect obj_up "><i className="fas fa-arrow-up mr-2" />ชึ้น</button>
                  <button type="button" className="btn btn-outline-info btn-rounded waves-effect obj_down"><i className="fas fa-arrow-down mr-2" />ลง</button>
                  <button type="button" className="btn btn-outline-info btn-rounded waves-effect  clear_canvas"><i className="fas fa-trash-alt mr-2" />ล้าง</button>
                  <button type="button" className="btn btn-outline-info btn-rounded waves-effect clear_obj"><i className="fas fa-eraser mr-2" />ลบ</button>
                  <button type="button" className="btn btn-outline-info btn-rounded waves-effect" onClick={DownloadCanvasAsImage}><i className="fas fa-download mr-2" />บันทึกรูป</button>
                </div>

                <ul className="nav nav-tabs" id="myTab" role="tablist">
                  <li className="nav-item">
                    <a className="nav-link active h3" id="text-tab" data-toggle="tab" href="#text" role="tab" aria-controls="text" aria-selected="true">อักษร</a>
                  </li>
                  <li className="nav-item">
                    <a className="nav-link h3" id="images-tab" data-toggle="tab" href="#images" role="tab" aria-controls="images" aria-selected="false">รูปภาพ</a>
                  </li>
                </ul>
                <div className="tab-content" id="myTabContent">
                  <div className="tab-pane fade show active " id="text" role="tabpanel" aria-labelledby="text-tab">
                    <div className="mt-3">
                      <div>
                        <button type="button" className="btn btn-outline-deep-purple  btn-block shadow-sm add_text  mb-1"><h4>เพิ่มตัวอักษร</h4></button>
                      </div>
                      <div className="hide_area" id="font_area">
                        <div>
                          <label htmlFor="text_color">สีตัวอักษร</label>
                          <input type="color" className="text_color ml-2" />
                        </div>
                        <div>
                          <label htmlFor="text_bg">พื้นหลังตัวอักษร</label>
                          <input type="color" className="text_bg ml-2" />
                        </div>
                        <div className="border mt-2 ex2 " >
                          <div className="row m-3">
                            <div className="col">
                              <div className="radio" >
                                <input autoComplete="off" type="radio" name="radio_position" id="value_1" defaultValue="Georgia" />
                                <label htmlFor="value_1" id="valueSet_1">Georgia</label>
                              </div>
                              <div className="radio">
                                <input autoComplete="off" type="radio" name="radio_position" id="value_2" defaultValue="Palatino Linotype" />
                                <label htmlFor="value_2" id="valueSet_2">Palatino Linotype</label>
                              </div>
                              <div className="radio">
                                <input autoComplete="off" type="radio" name="radio_position" id="value_3" defaultValue="Book Antiqua" />
                                <label htmlFor="value_3" id="valueSet_3">Book Antiqua</label>
                              </div>
                              <div className="radio">
                                <input autoComplete="off" type="radio" name="radio_position" id="value_4" defaultValue="Times New Roman" />
                                <label htmlFor="value_4" id="valueSet_4">Times New Roman</label>
                              </div>
                              <div className="radio">
                                <input autoComplete="off" type="radio" name="radio_position" id="value_5" defaultValue="Arial" />
                                <label htmlFor="value_5" id="valueSet_5">Arial</label>
                              </div>
                              <div className="radio">
                                <input autoComplete="off" type="radio" name="radio_position" id="value_6" defaultValue="Helvetica" />
                                <label htmlFor="value_6" id="valueSet_6">Helvetica</label>
                              </div>
                              <div className="radio">
                                <input autoComplete="off" type="radio" name="radio_position" id="value_7" defaultValue="Arial Black" />
                                <label htmlFor="value_7" id="valueSet_7">Arial Black</label>
                              </div>
                            </div>
                            <div className="col">
                              <div className="radio">
                                <input autoComplete="off" type="radio" name="radio_position" id="value_8" defaultValue="Impact" />
                                <label htmlFor="value_8" id="valueSet_8">Impact</label>
                              </div>
                              <div className="radio">
                                <input autoComplete="off" type="radio" name="radio_position" id="value_9" defaultValue="Lucida Sans Unicode" />
                                <label htmlFor="value_9" id="valueSet_9">Lucida Sans Unicode</label>
                              </div>
                              <div className="radio">
                                <input autoComplete="off" type="radio" name="radio_position" id="value_10" defaultValue="Tahoma" />
                                <label htmlFor="value_10" id="valueSet_10">Tahoma</label>
                              </div>
                              <div className="radio">
                                <input autoComplete="off" type="radio" name="radio_position" id="value_11" defaultValue="Verdana" />
                                <label htmlFor="value_11" id="valueSet_11">Verdana</label>
                              </div>
                              <div className="radio">
                                <input autoComplete="off" type="radio" name="radio_position" id="value_12" defaultValue="Courier New" />
                                <label htmlFor="value_12" id="valueSet_12">Courier New</label>
                              </div>
                              <div className="radio">
                                <input autoComplete="off" type="radio" name="radio_position" id="value_13" defaultValue="Lucida Console" />
                                <label htmlFor="value_13" id="valueSet_13">Lucida Console</label>
                              </div>
                              <div className="radio">
                                <input autoComplete="off" type="radio" name="radio_position" id="value_14" defaultValue="initial" />
                                <label htmlFor="value_14" id="valueSet_14">initial</label>
                              </div>

                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="tab-pane fade" id="images" role="tabpanel" aria-labelledby="images-tab">
                    <div className="mt-3" id="img_area">
                      <div className="custom-file">
                        <input type="file" className="custom-file-input" id="file" required />
                        <label className="custom-file-label" id="label-filename" >กรุณาเลือกรูป</label>
                      </div>
                      <div className="border mt-2 ex3" >
                        <div style={{ height: "1000px" }} className="m-3">
                          <div className="row justify-content-around">
                            {Array(20).fill("").map((x, i) => (
                              <div key={i} className="col-3 p-1 text-center border border-danger m-2 img-make"
                                style={{ display: "flex", justifyContent: "center", alignItems: "center" }}>
                                <img src={`../static/img-create/${i + 1}.png`} id="img" alt="" style={{ width: "80%", cursor: "pointer" }} />
                              </div>
                            ))}
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="mt-3" id="img_design_area" style={{ display: "none" }}>
                      {/* <div>
                        <label htmlFor="img_color">Image color</label>
                        <input type="color" className="img_color ml-2" />
                      </div> */}
                      <div>
                        <label htmlFor="text_bg">Background Image color</label>
                        <input type="color" className="text_bg ml-2" />
                      </div>
                    </div>
                  </div>
                </div>


              </div>
            </div>
          </div>
        </div>
        <div className="mt-5">
          <div>
            <Link href="/products">
              <button type="button" className="btn btn-outline-primary btn-style waves-effect"><i className="fas fa-arrow-left mr-2"></i>ไปหน้าสินค้า</button>
            </Link>
            <Link href="/payment/payment_production">
              <button type="button" className="btn btn-outline-primary btn-style waves-effect float-right">ไปหน้าสั่งผลิต<i className="fas fa-arrow-right ml-2"></i></button>
            </Link>
          </div>
        </div>
      </div>
      <style jsx>{`
    

          .btn-style{
            border-radius: 25px;
          }
          div.ex3 {
            width: auto;
            height: 252px;
            overflow: auto;
          }
          div.ex2 {
            width: auto;
            height: 139px;
            overflow: auto;
          }
          .hide_area{
            display: none;
          }
          #canvas{
            border: solid 1px blue;  
            width: 100%;
          }
      `}</style>
      {/* <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
      <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.4/umd/popper.min.js"></script>
      <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/js/bootstrap.min.js"></script>
      <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.10.1/js/mdb.min.js"></script> */}
    </Layout >
  );
}

export default create;
