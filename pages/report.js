import React, { useEffect, useState } from 'react';
import { useRouter } from 'next/router';
import axios from 'axios';
import moment from "moment";

const report = () => {
  const router = useRouter();
  const [dataP, setDataP] = useState([]);

  useEffect(() => {
    if (router.query) {
      getAllpayment();
    } else {
      router.push('/management');
    }
  }, []);

  const getAllpayment = () => {

    axios.get(`${process.env.host_api}/payment/order_list/all_order`, {
      // http://ss-api.inventage.co
    })
      .then(response => {
        getDataReport(response.data);

      })
      .catch(function (error) {
        console.log(error);
      });
  }
  const getDataReport = (obj) => {
    // console.log(router);
    if (obj) {
      // console.log(router.query.startDate);
      // e.preventDefault();
      var startDate = moment(router.query.startDate, "YYYY-MM-DD");
      // console.log(startDate);
      var endDate = moment(router.query.endDate, "YYYY-MM-DD");
      const funcStartDate = (date, startDate) => startDate._isValid ? startDate <= date : true;
      const funcEndDate = (date, endDate) => endDate._isValid ? date <= endDate : true;

      let getNewdata = obj.filter((obj) => {
        let date = moment(obj.payment_date.split(',')[0], "DD/MM/YYYY");
        return (funcStartDate(date, startDate) && funcEndDate(date, endDate))
      })
      setDataP(getNewdata);
      console.log(getNewdata);
    }
  }

  const printDiv = () => {
    // var printContents = document.getElementById("print-invoice").innerHTML;
    // var originalContents = document.body.innerHTML;

    // document.body.innerHTML = printContents;

    window.print();

    // document.body.innerHTML = originalContents;

  }
  return (
    <div>
      <div id="button" style={{ display: "flex", justifyContent: "center" }}>
        <button id="btn" onClick={printDiv} type="button" className="btn btn-outline-warning" style={{ borderRadius: "25px" }} > Print Invoice </button>
      </div>
      <div id="print-invoice" className="shadow-box mt-4 mb-4" style={{ backgroundColor: "rgb(243, 243, 243)", width: "210mm", margin: "auto", padding: "5mm" }}>
        <div className="margin-top">
          <div style={{ display: "flex", justifyContent: "center" }}>
            <p className="ml-2" style={{ color: "#07eee5", fontWeight: "bold" }}>รายงานการสั่งผลิตสินค้า</p>
          </div>
          <hr className="head-hr" />
          <div>
            ยอดการสั่งผลิดสินค้า {router.query.startDate && router.query.endDate ?
              router.query.startDate + " ถึง " + router.query.endDate :
              router.query.startDate ? " > " + router.query.startDate :
                router.query.endDate ? " < " + router.query.endDate : "ทั้งหมด"}
          </div>
          <hr className="head-hr" />
          {router.query.type == "production" && dataP.length ?
            <table className="table table-striped mt-3">
              <thead>
                <tr>
                  <th scope="col">#</th>
                  <th scope="col" width="50%">วันที่</th>
                  {/* <th scope="col">ราคา/ชิ้น</th> */}
                  <th scope="col">จำนวน</th>
                  <th scope="col">ราคารวม</th>
                </tr>
              </thead>
              <tbody>

                {// <tr>
                  //   <td>1</td>
                  //   <td>สินค้าการสั่งผลิต</td>
                  //   <td>{dataP.filter(obj => obj.status == "5" && obj.type_p == "2").reduce((a, b) => { return (a + Number(b.allquantity)) }, 0).toLocaleString()}</td>
                  //   <td>{dataP.filter(obj => obj.status == "5" && obj.type_p == "2").reduce((a, b) => a + (b.allquantity * b.production_price), 0).toLocaleString()}</td>
                  // </tr>
                }
                {dataP.map((data, i) => (
                  data.type_p == 2 && data.status == "5" ?
                    <tr key={i}>
                      <th >{i + 1}</th>
                      <td >{data.payment_date}</td>
                      <td >{data.quantity_payment}</td>
                      <td >{(data.production_price * data.quantity_payment).toLocaleString()}</td>
                    </tr>
                    : ""
                ))}



              </tbody>

              <tfoot>
                <tr>
                  <td colSpan="2">ผลรวม</td>
                  <td >
                    {
                      dataP.filter(data => data.type_p == 2 && data.status == "5").reduce((a, b) => a + b.quantity_payment, 0)

                      // dataP.filter(data => data.type_p == 2 && data.status == "5").reduce((a, b) => Number(a.quantity_payment) + Number(b.quantity_payment))
                    }

                  </td>
                  <td>

                    {dataP.filter(data => data.type_p == 2 && data.status == "5").reduce((a, b) => a + (b.production_price * b.quantity_payment), 0).toLocaleString()}

                  </td>
                </tr>
              </tfoot>

            </table>
            : ""
          }
          {router.query.type == "buy" && dataP.length ?
            <table className="table table-striped mt-3">
              <thead>
                <tr>
                  <th scope="col">#</th>
                  <th scope="col" width="50%">วันที่</th>
                  {/* <th scope="col">ราคา/ชิ้น</th> */}
                  <th scope="col">จำนวน</th>
                  <th scope="col">ราคารวม</th>
                </tr>
              </thead>
              <tbody>

                {// <tr>
                  //   <td>1</td>
                  //   <td>สินค้าการสั่งผลิต</td>
                  //   <td>{dataP.filter(obj => obj.status == "5" && obj.type_p == "2").reduce((a, b) => { return (a + Number(b.allquantity)) }, 0).toLocaleString()}</td>
                  //   <td>{dataP.filter(obj => obj.status == "5" && obj.type_p == "2").reduce((a, b) => a + (b.allquantity * b.production_price), 0).toLocaleString()}</td>
                  // </tr>
                }
                {dataP.map((data, i) => (
                  data.type_p == 1 && data.status == "5" ?
                    <tr key={i}>
                      <th >{i}</th>
                      <td >{data.payment_date}</td>
                      <td >{data.quantity}</td>
                      <td >{(data.price * data.quantity).toLocaleString()}</td>
                    </tr>
                    : ""
                ))}



              </tbody>

              <tfoot>
                <tr>
                  <td colSpan="2">ผลรวม</td>
                  <td >
                    {
                      dataP.filter(data => data.type_p == 1 && data.status == "5").reduce((a, b) => a + b.quantity, 0)

                      // dataP.filter(data => data.type_p == 2 && data.status == "5").reduce((a, b) => Number(a.quantity_payment) + Number(b.quantity_payment))
                    }

                  </td>
                  <td>

                    {dataP.filter(data => data.type_p == 1 && data.status == "5").reduce((a, b) => a + (b.price * b.quantity), 0).toLocaleString()}

                  </td>
                </tr>
              </tfoot>

            </table>
            : ""
          }
        </div>
      </div>
      <style jsx>{`
            @media print {
              body * {
                visibility: hidden;
              }
              #button *{
                visibility: hidden;
              }
              #print-invoice, #print-invoice * {
                visibility: visible;
              }
              #print-invoice {
                position: absolute;
                left: 0;
                top: 0;
                right: 0;
                bottom: 0;
                width: unset !important;
                height: unset !important;
                box-shadow: none;
                margin: 0 !important;
              }
            }
            `}</style>
    </div>
  );
}

export default report;
