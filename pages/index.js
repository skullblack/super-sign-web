import React, { useState, useEffect } from "react";
import Layout from "../components/Layout";
import Head from "next/head";
import { useRouter } from 'next/router';
import Link from "next/link";
import Footer from '../components/footer';
const axios = require('axios');

function IndexPage() {
  const router = useRouter();
  const [dataProduct, setDataproduct] = useState([]);
  const [countProduct, setCountproduct] = useState("");
  const [dataui, setDataui] = useState([]);
  const [button, setButton] = useState(false);
  useEffect(() => {
    getdataUi();
    getSto();

  }, [])
  const getSto = () => {
    axios.get(`${process.env.host_api}/storage/s2`, {
      // http://ss-api.inventage.co

    })
      .then(response => {
        setDataproduct(response.data);
        console.log(response.data);


      })
      .catch(function (error) {
        console.log(error);

      });
  }
  const getdataUi = () => {
    axios.get(`${process.env.host_api}/indexui`)
      .then(response => {

        setDataui(response.data);

      })
      .catch(function (error) {
        console.log(error);

      });

  }
  const single_product = (id) => {
    router.push({
      pathname: '/single_product',
      query: {
        product_id: id
      },
    })
  }
  const addCart = (e) => {
    var productid = e.target.productId.value;
    var productimg = e.target.productImg.value;
    var productname = e.target.productName.value;
    var productdetails = e.target.productDetails.value;
    var productprice = e.target.productPrice.value;

    e.preventDefault();

    $(e.target.submit).slideUp(300).slideDown(300);

    if (!localStorage.getItem("cart")) {
      var carts = [];
      var setitem = {
        id: productid,
        img: productimg,
        name: productname,
        details: productdetails,
        price: productprice,
        count: 1
      };
      carts.push(setitem);
      localStorage.setItem("cart", JSON.stringify(carts));
    } else {
      var stoCart = JSON.parse(localStorage.getItem("cart"));
      var objIndex = stoCart.findIndex((obj => obj.id == productid));

      if (objIndex >= 0) {

        stoCart[objIndex].count += 1;
        localStorage.setItem("cart", JSON.stringify(stoCart));
      } else {
        var setitem = {
          id: productid,
          img: productimg,
          name: productname,
          details: productdetails,
          price: productprice,
          count: 1
        };
        stoCart.push(setitem);
        localStorage.setItem("cart", JSON.stringify(stoCart));

      }
    }
    var stoCartcheck = JSON.parse(localStorage.getItem("cart"));
    let sum = stoCartcheck.map(o => o.count).reduce((a, c) => { return a + c });
    setCountproduct(sum);
  }
  return (
    <Layout
      page="index"
      count={countProduct}
    >
      <Head>
        <title>Super sign</title>
      </Head>
      <div className="jumbotron card card-image mt-5 "
        style={{
          // backgroundImage: 'url(../static/images/body-img.jpg)'
          backgroundImage: 'url(../static/images/bg-index.png),url(../static/images/body-img.jpg)',
          backgroundPosition: "center",
          backgroundRepeat: "no-repeat",
          backgroundSize: "cover"
        }}>
        <div className="text-white text-center py-5 px-4 ">
          <div>
            <h2 className="card-title h1-responsive pt-3 mb-5 font-bold"><strong>{dataui.length ? dataui[0].text1 : ""}</strong></h2>
            <p className="mx-5 mb-5 d-block mx-auto" style={{ maxWidth: "750px" }}>
              {dataui.length ? dataui[0].text2 : ""}
            </p>
            <Link href="/products" >
              <a className={`btn ${button? "btn-success":"btn-outline-white"} btn-md`}
                onMouseEnter={()=>setButton(!button)}
                onMouseLeave={()=>setButton(!button)}><i className="fas fa-clone left" />ซื้อสินค้าเลย</a>
            </Link>
          </div>
        </div>
      </div>

      <div className="container mt-5">
        <div className="text-center">
          <h2 className="text-center font-weight-bold mb-4 pb-2">สินค้าใหม่</h2>
          {dataui.length ?
            <p className="text-center mx-auto w-responsive mb-5">
              {dataui[0].text3}
            </p> :
            <div className="spinner-border text-primary mb-5" role="status">
              <span className="sr-only">Loading...</span>
            </div>
          }
        </div>
        {dataProduct.map((prod, i) => (
          <section key={i} className="dark-grey-text">
            <form onSubmit={((e) => addCart(e))}>
              <input type="hidden" name="productId" defaultValue={prod.id} />
              <input type="hidden" name="productImg" defaultValue={prod.picture} />
              <input type="hidden" name="productName" defaultValue={prod.name} />
              <input type="hidden" name="productDetails" defaultValue={prod.details} />
              <input type="hidden" name="productPrice" defaultValue={prod.price} />
              <div className="row align-items-center">
                <div className={i % 2 == 0 ? `col-lg-5` : `col-lg-5 order-lg-1`}>
                  <div className="view overlay rounded z-depth-2 mb-lg-0 mb-4 d-flex justify-content-center">
                    <img className="img-fluid" src={prod.picture} alt="Sample image" style={{ maxHeight: "350px" }} />
                    <a>
                      <div className="mask rgba-white-slight" />
                    </a>
                  </div>
                </div>
                <div className="col-lg-7" >
                  <h4 className={i % 2 == 0 ? `font-weight-bold mb-3` : `font-weight-bold mb-3 d-flex justify-content-end`}><strong>{prod.name}</strong></h4>
                  <p className={i % 2 == 0 ? `crop-text` : `crop-text d-flex justify-content-end`}>{prod.details}</p>
                  <p className={i % 2 == 0 ? `` : ` d-flex justify-content-end`}><strong>ราคา : </strong> {prod.price.toFixed(2)} 	฿</p>
                  <p className={i % 2 == 0 ? `` : ` d-flex justify-content-end`}><strong>วันที่จำหน่าย</strong> , {prod.date_product}</p>
                  <div className={i % 2 == 0 ? `` : ` d-flex justify-content-end`}>
                    <a onClick={(() => single_product(prod.id))} className="btn btn-success btn-md btn-rounded mx-0">เพื่มเติม</a>
                    <button type="submit" className="btn btn-warning btn-md" name="submit"><i className="fas fa-cart-arrow-down" /></button>
                  </div>

                </div>
              </div>
            </form>
            <hr className="my-5" />
          </section>
        ))}
      </div>
      <Footer />

      <style jsx>{`
    .crop-text {
      -webkit-line-clamp: 3;
      overflow : hidden;
      text-overflow: ellipsis;
      display: -webkit-box;
      -webkit-box-orient: vertical;
    }
    .card-title {
      color: white;
      -webkit-text-fill-color: white;
      -webkit-text-stroke-width: 1px;
      -webkit-text-stroke-color: 	gray;
    }
    `}</style>

    </Layout>
  );
}

export default IndexPage;