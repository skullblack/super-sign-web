import React, { useState, useEffect } from 'react';
import { useRouter } from 'next/router';
import Link from 'next/link';
import Swal from 'sweetalert2';
import $ from "jquery";
import withReactContent from 'sweetalert2-react-content';
import { MDBDataTable, MDBTable, MDBTableHead, MDBTableBody } from 'mdbreact';

import moment from "moment";
import axios from 'axios';
// import view_payment from './payment/view_payment';

const management = () => {
  const MySwal = withReactContent(Swal);
  const router = useRouter()
  const [scrollY, setScrollY] = useState();
  const [addClassform, setaddClassform] = useState(false);
  const [datausers, setDatauser] = useState([]);
  const [dataproduct, setDataproduct] = useState([]);
  const [dataAllpayment, setDataallpayment] = useState([]);
  const [typeProduct, setTypeproduct] = useState([]);
  const [dataui, setDataui] = useState([]);
  const [btnsaveindexuitoggle, setBtnsaveindexuitoggle] = useState(false);
  const [showTable, setShowTable] = useState([]);
  const [searchBuyData, setSearchBuyData] = useState([]);
  const [searchProdtionData, setSearchProdtionData] = useState([]);


  useEffect(() => {
    checkstoragemanag();
    getdataUi();

    $(document).on('input', '#formindexui textarea', function (e) {

      var form = $(e.target).parents('#formindexui');
      if ((form[0].header.value != form[0].headerCheck.value) ||
        (form[0].detailsHeader.value != form[0].detailsHeadercheck.value) ||
        (form[0].detailsNewproduct.value != form[0].detailsNewproductcheck.value)) {
        setBtnsaveindexuitoggle(true);
      } else {
        setBtnsaveindexuitoggle(false);
      }



    });

  }, [])
  const searchBuy = (e) => {
    e.preventDefault();
    var startDate = moment(e.target.firstdate.value, "YYYY-MM-DD");
    var endDate = moment(e.target.lastdate.value, "YYYY-MM-DD");
    const funcStartDate = (date, startDate) => startDate._isValid ? startDate <= date : true;
    const funcEndDate = (date, endDate) => endDate._isValid ? date <= endDate : true;

    let getNewdata = dataAllpayment.filter((obj) => {
      let date = moment(obj.payment_date.split(',')[0], "DD/MM/YYYY");
      return (funcStartDate(date, startDate) && funcEndDate(date, endDate))
    }
    )
    setSearchBuyData(getNewdata);
  }

  const searchProduction = (e) => {
    e.preventDefault();
    var startDate = moment(e.target.firstdate.value, "YYYY-MM-DD");
    // console.log(startDate);
    var endDate = moment(e.target.lastdate.value, "YYYY-MM-DD");
    const funcStartDate = (date, startDate) => startDate._isValid ? startDate <= date : true;
    const funcEndDate = (date, endDate) => endDate._isValid ? date <= endDate : true;

    let getNewdata = dataAllpayment.filter((obj) => {
      let date = moment(obj.payment_date.split(',')[0], "DD/MM/YYYY");
      return (funcStartDate(date, startDate) && funcEndDate(date, endDate))
    }
    )
    setSearchProdtionData(getNewdata);
  }

  const setShow = (id) => {
    if (id == 0) {
      setShowTable(dataproduct);
    } else {
      var result = dataproduct.filter(obj => obj.type_id === id);
      setShowTable(result);
    }

  }

  const gettypeProduct = () => {
    axios.get(`${process.env.host_api}/storage/type`, {

    })
      .then(response => {

        setTypeproduct(response.data);


      })
      .catch(function (error) {
        console.log(error);

      });
  }

  useEffect(() => {
    function watchScroll() {
      window.addEventListener("scroll", logit);
    }
    watchScroll();
    // Remove listener (like componentWillUnmount)
    return () => {
      window.removeEventListener("scroll", logit);
    };
  });

  const getdataUi = () => {
    axios.get(`${process.env.host_api}/indexui`)
      .then(response => {

        setDataui(response.data);
        $(".addclass").addClass("active");


      })
      .catch(function (error) {
        console.log(error);

      });

  }
  const saveIndexui = (e) => {
    MySwal.fire({
      title: 'กำลังโหลดข้อมูล',
      showConfirmButton: false,
      onOpen: () => {
        MySwal.showLoading();
      }
    })
    e.preventDefault();
    console.log(e);
    axios.post(`${process.env.host_api}/indexui/alltext`, {
      // http://ss-api.inventage.co
      text1: e.target.header.value,
      text2: e.target.detailsHeader.value,
      text3: e.target.detailsNewproduct.value
    })
      .then(response => {
        MySwal.fire({
          icon: 'success',
          title: 'บันทึกสำเร็จ',
          showConfirmButton: false,
          timer: 1500
        })
        console.log(response.data);

      })
      .catch(function (error) {
        MySwal.fire({
          icon: 'error',
          title: 'บันทึกผิดพลาด',
        })
        console.log(error);
      });


  }

  const checkstoragemanag = () => {
    if (localStorage.getItem("Status") != "admin") {
      router.push('/');
    } else {
      getAllpayment();
      getUser();
      getProductall();
      gettypeProduct();
    }

  }

  const logit = () => {
    if (window.pageYOffset > 70) {
      setScrollY(true);
    } else {
      setScrollY(false);
    }

  }

  const getUser = () => {
    var username = localStorage.getItem("Username");
    var password = localStorage.getItem("Password")
    axios.get(`${process.env.host_api}/user`, {
      // http://ss-api.inventage.co
      headers: {
        Username: username,
        Password: password
      }

    })
      .then(response => {

        setDatauser(response.data);

      })
      .catch(function (error) {
        console.log(error);

      });
  }

  // const logout = () => {
  //   let keysToRemove = [
  //     "Id", "Firstname", "Lastname",
  //     "Username", "Password", "Address",
  //     "Tel", "Email", "Status"];

  //   keysToRemove.map(key => {
  //     localStorage.removeItem(key);
  //   })

  //   router.push('/login_register')
  // }

  const getProductall = () => {
    MySwal.fire({
      title: 'กำลังโหลดข้อมูล',
      showConfirmButton: false,
      onOpen: () => {
        MySwal.showLoading();
      }
    })
    axios.get(`${process.env.host_api}/storage`, {
      // http://ss-api.inventage.co
    })
      .then(response => {
        setDataproduct(response.data);
        setShowTable(response.data);

        MySwal.close();
      })
      .catch(function (error) {
        MySwal.fire({
          icon: 'error',
          title: 'บันทึกผิดพลาด',
        })
        console.log(error);
      });
  }
  const getAllpayment = () => {

    axios.get(`${process.env.host_api}/payment/order_list/all_order`, {
      // http://ss-api.inventage.co
    })
      .then(response => {
        console.log(response.data);
        setSearchBuyData(response.data);
        setSearchProdtionData(response.data);
        setDataallpayment(response.data);
      })
      .catch(function (error) {
        console.log(error);
      });
  }

  // add product
  const addProductsubmit = () => {
    MySwal.fire({
      title: 'กำลังบันทึก',
      showConfirmButton: false,
      onOpen: () => {
        MySwal.showLoading();
      }
    })
    var productName = document.getElementById("productName").value;
    var Price = document.getElementById("Price").value;
    var Quantity = document.getElementById("Quantity").value;
    var Category = document.getElementById("Category").value;
    var Details = document.getElementById("Details").value;
    var File = document.getElementById("File").files;
    if (!productName || !Price || !Quantity || !Category || !Details || !File) {
      setaddClassform(true);
    } else {
      const formData = new FormData();
      formData.append("name", productName);
      formData.append("price", Price);
      formData.append("quantity", Quantity);
      formData.append("type", Category);
      formData.append("details", Details);
      formData.append("file", File[0]);

      axios.put(`${process.env.host_api}/storage`, formData, {
        headers: {
          'Content-Type': "multipart/form-data"
        }
      })
        .then(response => {
          console.log(response);
          var form = document.getElementById("formAddproduct");
          form.reset();
          setaddClassform(false);
          MySwal.fire({
            icon: 'success',
            title: 'บันทึกสำเร็จ',
            showConfirmButton: false,
            timer: 1500
          })
          getProductall();
        })
        .catch(function (error) {
          MySwal.fire({
            icon: 'error',
            title: 'บันทึกผิดพลาด',
          })
          console.log(error);
        });

    }
  }
  // user control
  const modaleditUser = (id) => {

    let user = datausers.find(user => user.id == id);
    let listedit = [
      "Id",
      "Username",
      "Password",
      "Firstname",
      "Lastname",
      "Tel",
      "Address",
      "Email"
    ]

    listedit.forEach(x => {
      if (user[x.toLowerCase()]) {
        document.getElementById(`edit${x}`).value = user[x.toLowerCase()];
        document.getElementById(`edit${x}`).nextElementSibling.classList.add("active");
      } else {
        document.getElementById(`edit${x}`).value = user[x.toLowerCase()];
        document.getElementById(`edit${x}`).nextElementSibling.classList.remove("active");
      }
    });
    $("#modaleditUser").modal();

  }

  const saveEdituser = (e) => {
    e.preventDefault();


    axios.post(`${process.env.host_api}/user`, {
      // http://ss-api.inventage.co
      id: e.target.editId.value,
      username: e.target.editUsername.value,
      password: e.target.editPassword.value,
      firstname: e.target.editFirstname.value,
      lastname: e.target.editLastname.value,
      email: e.target.editEmail.value,
      tel: e.target.editTel.value,
      address: e.target.editAddress.value
    })
      .then(response => {
        console.log(response.data);

        getUser();
        $('#modaleditUser').modal('hide');
      })
      .catch(function (error) {
        console.log(error);
      });
  }

  const modalsearchUser = (id) => {

    let user = datausers.find(user => user.id == id);
    let listedit = [
      "Username",
      "Firstname",
      "Lastname",
      "Tel",
      "Address",
      "Email"
    ]

    listedit.forEach(x => {
      document.getElementById(`search${x}`).value = user[x.toLowerCase()];

    });
    $("#searchUser").modal();

  }
  const deleteUser = (id) => {
    MySwal.fire({
      title: 'กำลังลบข้อมูล',
      showConfirmButton: false,
      onOpen: () => {
        MySwal.showLoading();
      }
    })
    console.log(id);

    axios.delete(`${process.env.host_api}/user/${id}`)
      .then(response => {
        console.log(response);
        Swal.fire({
          icon: 'success',
          title: 'ลบสำเร็จ',
          showConfirmButton: false,
          timer: 1500
        }).then(() => {
          getUser();
          MySwal.close();
        })
      })
      .catch(function (error) {
        Swal.fire({
          icon: 'error',
          title: 'ลบผิดพลาด',
        })
        console.log(error);
      });
  }
  // product control
  const modaleditProduct = (id) => {
    let prod = dataproduct.find(prod => prod.id == id);
    let listedit = [
      "Id",
      "Name",
      "Picture",
      "Price",
      "Quantity",
      "Details"
    ]

    listedit.forEach(x => {
      if (prod[x.toLowerCase()]) {
        if (x == "Picture") {
          document.getElementById(`editproduct${x}`).src = prod[x.toLowerCase()];
        } else {
          document.getElementById(`editproduct${x}`).value = prod[x.toLowerCase()];
          document.getElementById(`editproduct${x}`).nextElementSibling.classList.add("active");
        }
      } else {
        document.getElementById(`editproduct${x}`).value = prod[x.toLowerCase()];
        document.getElementById(`editproduct${x}`).nextElementSibling.classList.remove("active");
      }
    });
    $("#modaleditProduct").modal();
  }
  const saveEditproduct = (e) => {
    e.preventDefault();


    axios.post(`${process.env.host_api}/storage`, {
      // http://ss-api.inventage.co
      id: e.target.editproductId.value,
      name: e.target.editproductName.value,
      price: e.target.editproductPrice.value,
      quantity: e.target.editproductQuantity.value,
      details: e.target.editproductDetails.value
    })
      .then(response => {
        console.log(response.data);

        getProductall();
        $('#modaleditProduct').modal('hide');
      })
      .catch(function (error) {
        console.log(error);
      });
  }
  const modalSearchproduct = (id) => {
    let prod = dataproduct.find(prod => prod.id == id);

    let listedit = [
      "Picture",
      "Name",
      "Price",
      "Quantity",
      "Type_pd",
      "Details"
    ]

    listedit.forEach(x => {

      if (x == "Picture") {
        document.getElementById(`search${x}`).src = prod[x.toLowerCase()];
      } else {
        document.getElementById(`search${x}`).value = prod[x.toLowerCase()];
      }
    });
    $("#searchProduct").modal();
  }
  const deleteProduct = (id) => {
    MySwal.fire({
      title: 'กำลังลบข้อมูล',
      showConfirmButton: false,
      onOpen: () => {
        MySwal.showLoading();
      }
    })
    console.log(id);

    axios.delete(`${process.env.host_api}/storage/${id}`)
      .then(response => {
        Swal.fire({
          icon: 'success',
          title: 'ลบสำเร็จ',
          showConfirmButton: false,
          timer: 1500
        }).then(() => {
          getProductall();
          MySwal.close();
        })
        console.log(response);

      })
      .catch(function (error) {
        Swal.fire({
          icon: 'error',
          title: 'ลบผิดพลาด',
        })
        console.log(error);
      });
  }
  const check_payment = (e) => {

    readItem(e);

    router.push({
      pathname: '/payment/check_payment',
      query: { order_id: e.order_id, type_p: e.type_p },
    })

  }

  const readItem = (e) => {
    console.log(e.order_id);

    axios.post(`${process.env.host_api}/payment/up_read`, {
      order_id: e.order_id
    })
      .then(response => {
        console.log(response);

      })
      .catch(function (error) {
        console.log(error);
      });
  }

  const view_payment = (e) => {

    router.push({
      pathname: '/payment/view_payment',
      query: {
        order_id: e.order_id,
        type_p: e.type_p,
        status: e.status
      },
    })

  }

  const assess_modal = (obj) => {
    console.log(obj);
    document.getElementById(`assessPicture`).src = obj.design_picture;

    document.getElementById(`assessId`).value = obj.order_id;

    document.getElementById(`assessWidth`).value = obj.width;
    document.getElementById(`assessWidth`).nextElementSibling.classList.add("active");
    document.getElementById(`assessHeight`).value = obj.height;
    document.getElementById(`assessHeight`).nextElementSibling.classList.add("active");
    document.getElementById(`assessMaterial`).value = obj.materialtype;
    document.getElementById(`assessMaterial`).nextElementSibling.classList.add("active");
    document.getElementById(`assessDetails`).value = obj.detail;
    document.getElementById(`assessDetails`).nextElementSibling.classList.add("active");
    document.getElementById(`assessAddress`).value = obj.user_address;
    document.getElementById(`assessAddress`).nextElementSibling.classList.add("active");

    $('#assess').modal('show')
  }
  const saveAssess = (e) => {
    e.preventDefault();
    axios.post(`${process.env.host_api}/payment/up_production_price`, {
      // http://ss-api.inventage.co
      order_id: e.target.assessId.value,
      price: e.target.assessPrice.value,
      status: "2"
    })
      .then(response => {
        console.log(response.data);

        getAllpayment();
        $('#assess').modal('hide')
      })
      .catch(function (error) {
        console.log(error);
      });



  }

  const addtype_product = (e) => {
    e.preventDefault();
    MySwal.fire({
      title: 'กำลังบันทึก',
      showConfirmButton: false,
      onOpen: () => {
        MySwal.showLoading();
      }
    })
    console.log(e.target.typeName.value);
    axios.put(`${process.env.host_api}/storage/type_product`, {
      // http://ss-api.inventage.co
      material: e.target.typeName.value
    })
      .then(response => {
        if (response.data == "repeatedly") {
          MySwal.fire({
            icon: 'error',
            title: 'วัสดุซ้ำ',
          })
        } else {
          MySwal.fire({
            icon: 'success',
            title: 'บันทึกสำเร็จ',
            showConfirmButton: false,
            timer: 1500
          })
        }

        console.log(response.data);

        gettypeProduct();
      })
      .catch(function (error) {
        MySwal.fire({
          icon: 'error',
          title: 'บันทึกผิดพลาด',
        })
        console.log(error);
      });

  }
  const editMaterial = (e) => {
    if (e.childNodes[2].childNodes[0].localName == "input") {
      console.log(e.childNodes[2].childNodes);

    } else {
      var fieldValue = e.childNodes[2].innerHTML;
      var input = `<input type="text" value="${fieldValue}"/>`;
      e.childNodes[2].innerHTML = input;
      e.childNodes[3].childNodes[0].classList.remove("d-none")
    }

  }

  const saveEditmaterial = (e, id, defaultType) => {

    var value = e.childNodes[2].childNodes[0].value;
    if (value.trim() != defaultType) {

      MySwal.fire({
        title: 'กำลังบันทึก',
        showConfirmButton: false,
        onOpen: () => {
          MySwal.showLoading();
        }
      })
      // e.childNodes[2].innerHTML = value;
      e.childNodes[3].childNodes[0].classList.add("d-none");
      axios.post(`${process.env.host_api}/storage/edit_material`, {
        // http://ss-api.inventage.co
        type_pd: value.trim(),
        id: id
      })
        .then(response => {
          gettypeProduct();
          MySwal.fire({
            icon: 'success',
            title: 'บันทึกสำเร็จ',
            showConfirmButton: false,
            timer: 1500
          })
        })
        .catch(function (error) {
          console.log(error);
        });
    } else {
      e.childNodes[3].childNodes[0].classList.add("d-none");
      e.childNodes[2].innerHTML = defaultType;
    }

  }

  const closeEditmaterial = (e, defaultType) => {
    e.childNodes[2].innerHTML = defaultType;
    e.childNodes[3].childNodes[0].classList.add("d-none");
  }

  const deleteMaterial = (e) => {
    MySwal.fire({
      title: 'กำลังลบ',
      showConfirmButton: false,
      onOpen: () => {
        MySwal.showLoading();
      }
    })
    axios.post(`${process.env.host_api}/storage/delete_material`, {
      // http://ss-api.inventage.co
      id: e.id
    })
      .then(response => {
        gettypeProduct();
        MySwal.fire({
          icon: 'success',
          title: 'ลบสำเร็จ',
          showConfirmButton: false,
          timer: 1500
        })
      })
      .catch(function (error) {
        MySwal.fire({
          icon: 'error',
          title: 'error'
        })
        console.log(error);
      });


  }

  const report = (data, f, l, type) => {
    console.log(data);
    router.push({
      pathname: '/report',
      query: { 
        startDate: f,
        endDate: l,
        type: type
       },
    })
  }

  return (
    <div>
      <style jsx>{`
                .margin-top{
                    margin-top: 90px;
                }
                @media screen and (min-width: 992px) { 
                    .fix-category .category {
                        position: fixed;
                        top: 70px;
                    }
                }
                
                .noresize {
                  resize: none; 
                }
                .noresize-prod {
                  resize: none;
                  height: 150px; 
                }
                .dataTables_filter{
                  float: right;
                }
                
                .nav-tabs .nav-link:not(.active) {
                  border-color: transparent !important;
                  border-radius: 15px;
                }
                .radius{
                    border-radius: 0 10px 10px 10px; 
                }
                .radius2{
                    border-radius: 10px; 
                }
                .box{
                    overflow : auto;
                }
              
            `}</style>


      {/* modal editProduct */}

      <div className="modal fade" id="modaleditProduct" tabIndex={-1} role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div className="modal-dialog modal-lg" role="document">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title" id="exampleModalLongTitle">แก้ไข สินค้า</h5>
              <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
              </button>
            </div>
            <div className="modal-body">
              <form onSubmit={((e) => saveEditproduct(e))}>
                <input type="hidden" className="form-control" id="editproductId" />
                <div className="form-row">
                  <div className="col-md-12 text-center">
                    <img id="editproductPicture" style={{ width: "50%", height: "auto" }} alt="Responsive image" />
                  </div>
                </div>
                <div className="form-row">
                  <div className="col-md-12">
                    <div className="md-form form-group">
                      <input type="text" className="form-control" min={1} id="editproductName" required />
                      <label htmlFor="editproductName">ชื่อสินค้า</label>
                    </div>
                  </div>
                </div>
                <div className="form-row">
                  <div className="col-md-6">
                    <div className="md-form form-group">
                      <input type="number" className="form-control" min={1} pattern="[0-9]" id="editproductPrice" required />
                      <label htmlFor="editproductPrice">ราคาสินค้า</label>
                    </div>
                  </div>
                  <div className="col-md-6">
                    <div className="md-form form-group">
                      <div className="md-form">
                        <input type="number" id="editproductQuantity" className="form-control " min="1" required />
                        <label htmlFor="editproductQuantity">จำนวนสินค้า</label>
                        <div className="invalid-feedback">กรุณาใส่จำนวนสินค้า</div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="row">
                  <div className="col-md-12">
                    <div className="md-form form-group">
                      <textarea id="editproductDetails" className="form-control md-textarea " />
                      <label htmlFor="editproductDetails">รายละเอียด</label>
                    </div>
                  </div>
                </div>
                <div className="modal-footer">
                  <button type="button" className="btn btn-outline-secondary  waves-effect mr-2" data-dismiss="modal">ยกเลิก</button>
                  <button className="btn btn-outline-success  waves-effect">บันทึก</button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>

      {/* modal searchproduct */}
      <div className="modal fade" id="searchProduct" tabIndex={-1} role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div className="modal-dialog modal-dialog-centered" role="document">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title" id="exampleModalLongTitle">สินค้า</h5>
              <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
              </button>
            </div>
            <div className="modal-body">

              <div className="form-group row">
                <div className="col-md-12 text-center">
                  <img id="searchPicture" style={{ width: "60%", height: "auto" }} alt="Responsive image" />
                </div>
              </div>
              <div className="form-group row">
                <label htmlFor="searchName" className="col-sm-2 col-form-label">ชื่อสินค้า</label>
                <div className="col-sm-10">
                  <input type="text" className="form-control border-0" id="searchName" disabled />
                </div>
              </div>
              <div className="form-group row">
                <label htmlFor="searchQuantity" className="col-sm-2 col-form-label">จำนวนสินค้า</label>
                <div className="col-sm-10">
                  <input type="text" className="form-control border-0" id="searchQuantity" disabled />
                </div>
              </div>
              <div className="form-group row">
                <label htmlFor="searchType_pd" className="col-sm-2 col-form-label">ประเภทสินค้า</label>
                <div className="col-sm-10">
                  <input type="text" className="form-control border-0" id="searchType_pd" disabled />
                </div>
              </div>
              <div className="form-group row">
                <label htmlFor="searchPrice" className="col-sm-2 col-form-label">ราคาสินค้า</label>
                <div className="col-sm-10">
                  <input type="text" className="form-control border-0" id="searchPrice" disabled />
                </div>
              </div>
              <div className="form-group row">
                <label htmlFor="searchDetails" className="col-sm-2 col-form-label">รายละเอียด</label>
                <div className="col-sm-10">
                  <textarea id="searchDetails" className="form-control md-textarea noresize-prod" disabled />
                </div>
              </div>

            </div>
            <div className="modal-footer">
              <button type="button" className="btn btn-outline-secondary  waves-effect mr-2" data-dismiss="modal">ปิด</button>
            </div>
          </div>
        </div>
      </div>

      {/* modal editUser */}

      <div className="modal fade" id="modaleditUser" tabIndex={-1} role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div className="modal-dialog modal-lg" role="document">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title" id="exampleModalLongTitle">แก้ไข ผู้ให้งาน</h5>
              <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
              </button>
            </div>
            <div className="modal-body">
              <form onSubmit={((e) => saveEdituser(e))}>
                <input type="hidden" className="form-control" id="editId" />
                <div className="form-row">
                  <div className="col-md-6">
                    <div className="md-form form-group">
                      <input type="text" className="form-control" id="editUsername" required />
                      <label htmlFor="editUsername">รหัสผู้ใช้</label>
                    </div>
                  </div>
                  <div className="col-md-6">
                    <div className="md-form form-group">
                      <input type="password" className="form-control" id="editPassword" required />
                      <label htmlFor="editPassword">พาสเวิร์ส</label>
                    </div>
                  </div>
                </div>
                <div className="form-row">
                  <div className="col-md-6">
                    <div className="md-form form-group">
                      <input type="text" className="form-control" id="editFirstname" />
                      <label htmlFor="editFirstname">ชื่อ</label>
                    </div>
                  </div>
                  <div className="col-md-6">
                    <div className="md-form form-group">
                      <input type="text" className="form-control" id="editLastname" />
                      <label htmlFor="editLastname">สกุล</label>
                    </div>
                  </div>
                </div>
                <div className="form-row">
                  <div className="col-md-6">
                    <div className="md-form form-group">
                      <input type="email" className="form-control" id="editEmail" />
                      <label htmlFor="editEmail">อีเมล์</label>
                    </div>
                  </div>
                  <div className="col-md-6">
                    <div className="md-form form-group">
                      <input type="text" className="form-control" id="editTel" />
                      <label htmlFor="editTel">เบอร์โทร</label>
                    </div>
                  </div>
                </div>
                <div className="row">
                  <div className="col-md-12">
                    <div className="md-form form-group">
                      <textarea id="editAddress" className="form-control md-textarea " />
                      <label htmlFor="editAddress">ที่อยู่</label>
                    </div>
                  </div>
                </div>
                <div className="modal-footer">
                  <button type="button" className="btn btn-outline-secondary  waves-effect mr-2" data-dismiss="modal">ยกเลิก</button>
                  <button className="btn btn-outline-success  waves-effect">บันทึก</button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
      {/* modal searchUser */}
      <div className="modal fade" id="searchUser" tabIndex={-1} role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div className="modal-dialog modal-dialog-centered" role="document">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title" id="exampleModalLongTitle">ผู้ให้งาน</h5>
              <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
              </button>
            </div>
            <div className="modal-body">

              <div className="form-group row">
                <label htmlFor="searchUsername" className="col-sm-2 col-form-label">รหัสผู้ใช้</label>
                <div className="col-sm-10">
                  <input type="text" className="form-control border-0" id="searchUsername" disabled />
                </div>
              </div>
              <div className="form-group row">
                <label htmlFor="searchFirstname" className="col-sm-2 col-form-label">ชื่อ</label>
                <div className="col-sm-10">
                  <input type="text" className="form-control border-0" id="searchFirstname" disabled />
                </div>
              </div>
              <div className="form-group row">
                <label htmlFor="searchLastname" className="col-sm-2 col-form-label">สกุล</label>
                <div className="col-sm-10">
                  <input type="text" className="form-control border-0" id="searchLastname" disabled />
                </div>
              </div>
              <div className="form-group row">
                <label htmlFor="searchEmail" className="col-sm-2 col-form-label">อีเมล์</label>
                <div className="col-sm-10">
                  <input type="text" className="form-control border-0" id="searchEmail" disabled />
                </div>
              </div>
              <div className="form-group row">
                <label htmlFor="searchTel" className="col-sm-2 col-form-label">เบอร์โทร</label>
                <div className="col-sm-10">
                  <input type="text" className="form-control border-0" id="searchTel" disabled />
                </div>
              </div>
              <div className="form-group row">
                <label htmlFor="searchAddress" className="col-sm-2 col-form-label">ที่อยู่</label>
                <div className="col-sm-10">
                  <textarea id="searchAddress" className="form-control md-textarea noresize" disabled />
                </div>
              </div>

            </div>
            <div className="modal-footer">
              <button type="button" className="btn btn-outline-secondary  waves-effect mr-2" data-dismiss="modal">ปิด</button>
            </div>
          </div>
        </div>
      </div>


      <div className="modal fade" id="assess" tabIndex={-1} role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div className="modal-dialog" role="document">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title" id="exampleModalLabel">ประเมินราคา</h5>
              <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
              </button>
            </div>
            <div className="modal-body">
              <form onSubmit={((e) => saveAssess(e))}>
                <input type="hidden" className="form-control" id="assessId" />
                <div className="form-row">
                  <div className="col-md-12 text-center">
                    <img id="assessPicture" style={{ width: "50%", height: "auto" }} alt="Responsive image" />
                  </div>
                </div>
                <div className="form-row">
                  <div className="col-md-6">
                    <div className="md-form form-group">
                      <input type="number" className="form-control" min={1} pattern="[0-9]" id="assessWidth" disabled required />
                      <label htmlFor="assessWidth">ความกว้าง</label>
                    </div>
                  </div>
                  <div className="col-md-6">
                    <div className="md-form form-group">
                      <input type="number" id="assessHeight" className="form-control " min="1" disabled required />
                      <label htmlFor="assessHeight">ความสูง</label>
                    </div>
                  </div>
                </div>
                <div className="form-row">
                  <div className="col-md-6">
                    <div className="md-form form-group">
                      <input type="number" className="form-control" min={1} pattern="[0-9]" id="assessPrice" required />
                      <label htmlFor="assessPrice">ราคา/ชิ้น</label>
                    </div>
                  </div>
                  <div className="col-md-6">
                    <div className="md-form form-group">
                      <input type="text" id="assessMaterial" className="form-control " disabled required />
                      <label htmlFor="assessMaterial">วัสดุ</label>
                    </div>
                  </div>
                </div>
                <div className="row">
                  <div className="col-md-12">
                    <div className="md-form form-group">
                      <textarea id="assessDetails" className="form-control md-textarea " disabled required />
                      <label htmlFor="assessDetails">รายละเอียด</label>
                    </div>
                  </div>
                </div>
                <div className="row">
                  <div className="col-md-12">
                    <div className="md-form form-group">
                      <textarea id="assessAddress" className="form-control md-textarea " disabled required />
                      <label htmlFor="assessAddress">ที่อยู่</label>
                    </div>
                  </div>
                </div>
                <div className="modal-footer">
                  <button type="button" className="btn btn-outline-secondary  waves-effect mr-2" data-dismiss="modal">ยกเลิก</button>
                  <button className="btn btn-outline-success  waves-effect">บันทึก</button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>


      <div className={scrollY ? " fix-category" : ""} >
        <nav className="navbar navbar-dark primary-color fixed-top">
          <Link href="/">
            <a className="navbar-brand" >
              <img src="../static/logo/logo-super-sign.png" height={30} className="d-inline-block align-top" alt="mdb logo" />
            </a>
          </Link>
        </nav>

        <div className="container card border-success mb-5 margin-top">
          <div className="row my-5">
            <div className=" col-lg-3 col-xl-2">
              <div className="category">
                <div className="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                  <a className="nav-link active" id="v-pills-home-tab" data-toggle="pill" href="#v-pills-home" role="tab" aria-controls="v-pills-home" aria-selected="true"><i className="fas fa-clipboard-list pr-1"></i>จัดการสินค้า</a>
                  <a className="nav-link" id="v-pills-profile-tab" data-toggle="pill" href="#v-pills-profile" role="tab" aria-controls="v-pills-profile" aria-selected="false"><i className="fas fa-users-cog pr-1"></i>จัดการผู้ใช้</a>
                  <a className="nav-link" id="v-pills-ui-tab" data-toggle="pill" href="#v-pills-ui" role="tab" aria-controls="v-pills-ui" aria-selected="false"><i className="fab fa-elementor pr-1"></i>จัดการหน้าหลัก</a>
                  <a className="nav-link" id="v-pills-messages-tab" data-toggle="pill" href="#v-pills-messages" role="tab" aria-controls="v-pills-messages" aria-selected="false">
                    <i className="fas fa-file-download pr-1"></i>
                    การสั่งผลิต<span className="badge badge-danger ml-2">{dataAllpayment.filter(o => o.type_p == '2' && o.status == '1' && o.read == '1').length ? dataAllpayment.filter(o => o.type_p == '2' && o.status == '1').length : ''}</span></a>
                  <a className="nav-link" id="v-pills-settings-tab" data-toggle="pill" href="#v-pills-settings" role="tab" aria-controls="v-pills-settings" aria-selected="false">
                    <i className="fas fa-file-invoice-dollar pr-1"></i>
                การสั่งซื้อ<span className="badge badge-danger ml-2">{dataAllpayment.filter(o => o.type_p == '1' && o.status == '3' && o.read == '1').length ? dataAllpayment.filter(o => o.type_p == '1' && o.status == '3').length : ''}</span></a>
                </div>
              </div>
            </div>
            <div className=" col-lg-9 col-xl-10">
              <div className="tab-content" id="v-pills-tabContent">
                <div className="tab-pane fade show active" id="v-pills-home" role="tabpanel" aria-labelledby="v-pills-home-tab">
                  <ul className="nav nav-tabs" id="myTab" role="tablist">
                    <li className="nav-item">
                      <a className="nav-link active " id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">เพิ่มสินค้า</a>
                    </li>
                    <li className="nav-item">
                      <a className="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">แก้ไข สินค้า</a>
                    </li>
                    <li className="nav-item">
                      <a className="nav-link" id="material-tab" data-toggle="tab" href="#material" role="tab" aria-controls="material" aria-selected="false">แก้ไข ประเภทสินค้า</a>
                    </li>
                  </ul>
                  <div className="tab-content" id="myTabContent">
                    <div className="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">

                      <div className="my-3 card">
                        <h5 className="card-header info-color white-text text-center py-4">
                          <strong>เพิ่มสินค้า</strong>
                        </h5>
                        <div className="card-body px-lg-5 pt-0">
                          {/* ใช้ classnmae was-validated */}
                          <form id="formAddproduct" className={addClassform ? "was-validated" : ""} style={{ color: '#757575' }} >
                            <div className="form-row ">
                              <div className="col-md-12">
                                <div className="md-form form-group">
                                  <input type="text" className="form-control " id="productName" required />
                                  <label className="custom-input-label" htmlFor="productName">ชื่อสินค้า</label>
                                  <div className="invalid-feedback">กรุณาใส่ชื่อสินค้า</div>
                                </div>
                              </div>
                              <div className="col-md-6">
                                <div className="md-form form-group">
                                  <input type="number" className="form-control " min="1" id="Price" required />
                                  <label htmlFor="Price">ราคาสินค้า</label>
                                  <div className="invalid-feedback">กรุณาใส่ราคาสินค้า</div>
                                </div>
                              </div>
                              <div className="col-md-6">
                                <div className="md-form form-group">
                                  <div className="md-form">
                                    <input type="number" id="Quantity" className="form-control " min="1" required />
                                    <label htmlFor="Quantity">จำนวนสินค้า</label>
                                    <div className="invalid-feedback">กรุณาใส่จำนวนสินค้า</div>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div className="form-group my-5">
                              <select className=" custom-select" id="Category" required>
                                <option value="">ประเภทสินค้า</option>
                                {typeProduct.map((type, i) => (
                                  <option key={i} value={type.id}>{type.type_pd}</option>
                                ))}
                              </select>
                            </div>
                            <div className="input-group my-5">
                              <div className="input-group-prepend">
                                <span className="input-group-text" id="custom-file">เพิ่ม</span>
                              </div>
                              <div className="custom-file">
                                <input type="file" className="custom-file-input" id="File" aria-describedby="custom-file" required />
                                <label className="custom-file-label " htmlFor="File">ชื่อรูปสินค้า</label>
                              </div>
                            </div>
                            <div className="md-form">
                              <textarea id="Details" className="form-control md-textarea " required />
                              <label htmlFor="Details">รายละเอียดสินค้า</label>
                            </div>
                            <button type="button" onClick={addProductsubmit} className="btn btn-outline-info btn-rounded btn-block z-depth-0 my-4 waves-effect" >บันทึก</button>
                          </form>
                        </div>
                      </div>
                      <div className="card">
                        <h5 className="card-header info-color white-text text-center py-4">
                          <strong>เพิ่มประเภทสินค้า</strong>
                        </h5>
                        <div className="card-body px-lg-5 pt-0">
                          <form onSubmit={((e) => addtype_product(e))} className="text-center" style={{ color: '#757575' }} >
                            <div className="md-form mt-3">
                              <input type="text" id="typeName" className="form-control" required />
                              <label htmlFor="typeName">ชื่อประเภทสินค้า</label>
                            </div>
                            <button className="btn btn-outline-info btn-rounded btn-block z-depth-0 my-4 waves-effect" type="submit">บันทึก</button>
                          </form>
                        </div>
                      </div>


                    </div>

                    <div className="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                      <div className="card m-3">
                        <div className="card-body">
                          <div className="table-responsive">
                            <table className="table product-table">
                              <thead className="mdb-color lighten-5">
                                <tr>
                                  <th />
                                  <th className="font-weight-bold">
                                    <strong>ชื่อสินค้า</strong>
                                  </th>
                                  <th />
                                  <th className="font-weight-bold px-3">

                                    <div className="dropdown">
                                      <a className=" dropdown-toggle p-0 m-0" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><strong>วัสดุสินค้า</strong></a>
                                      <div className="dropdown-menu dropdown-primary">
                                        <a className="dropdown-item" onClick={(() => setShow(0))}>วัสดุทั้งหมด</a>
                                        <hr />
                                        {typeProduct.map((obj, i) => (
                                          <a key={i} className="dropdown-item" onClick={(() => setShow(obj.id))}>{obj.type_pd}</a>
                                        ))}
                                      </div>
                                    </div>


                                  </th>
                                  <th className="font-weight-bold">
                                    <strong>จำนวนสินค้า</strong>
                                  </th>
                                  <th className="font-weight-bold">
                                    <strong>ราคาสินค้า</strong>
                                  </th>
                                  <th />
                                </tr>
                              </thead>
                              <tbody>
                                {showTable.map((prod, i) => (
                                  <tr key={i}>
                                    <th scope="row" style={{ width: "20%", height: "auto" }}>
                                      <img src={prod.picture} className="img-fluid z-depth-0" />
                                    </th>
                                    <td style={{ width: "25%" }}>
                                      <h5 className="mt-3">
                                        <strong>{prod.name}</strong>
                                      </h5>
                                    </td>
                                    <td />
                                    <td>{prod.type_pd}</td>
                                    <td className="font-weight-bold">
                                      <strong>{prod.quantity}</strong>
                                    </td>
                                    <td>{Number(prod.price).toLocaleString()}.00</td>
                                    <td>
                                      <div className="btn-group" role="group" aria-label="Basic example">
                                        <a onClick={(() => modalSearchproduct(prod.id))} className="mr-2 text-info"><h5><i className="fas fa-search fa-flip-horizontal"></i></h5></a>
                                        <a onClick={(() => modaleditProduct(prod.id))} className="mr-2 text-warning"><h5><i className="fas fa-wrench"></i></h5></a>
                                        <a onClick={(() => deleteProduct(prod.id))} className="text-danger"><h5><i className="fas fa-trash-alt"></i></h5></a>
                                      </div>
                                    </td>
                                  </tr>
                                ))}
                              </tbody>
                            </table>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="tab-pane fade" id="material" role="tabpanel" aria-labelledby="material-tab">
                      <div className="card m-3">
                        <div className="table-responsive card-body">
                          <table className="table product-table">
                            <caption>วัสดุสินค้า</caption>
                            <thead>
                              <tr className="grey lighten-4" >
                                <th scope="col"></th>
                                <th scope="col">#</th>
                                <th scope="col">ประเภทวัสดุสินค้า</th>
                                <th scope="col"></th>
                              </tr>
                            </thead>
                            <tbody>
                              {typeProduct.map((type, i) => (

                                <tr key={i} >

                                  <td scope="row" className="text-center" style={{ width: "15%" }}>
                                    <div className="btn-group " role="group" aria-label="Basic example">
                                      <a onClick={((e) => editMaterial(e.currentTarget.parentNode.parentNode.parentNode)
                                      )} className="mr-4 text-warning"><h5><i className="fas fa-pencil-alt"></i></h5></a>
                                      <a onClick={(() => deleteMaterial(type))} className="text-danger"><h5><i className="fas fa-trash-alt"></i></h5></a>
                                    </div>
                                  </td>
                                  <td>{i + 1}</td>
                                  <td id="typeName">{type.type_pd}</td>
                                  <td style={{ width: "15%" }} >
                                    <div className="btn-group d-none" role="group" aria-label="Basic example">
                                      <button onClick={((e) => saveEditmaterial(e.currentTarget.parentNode.parentNode.parentNode, type.id, type.type_pd)
                                      )} style={{ borderRadius: "50px" }} type="button" className="btn btn-default btn-sm px-3 mr-3"><i className="fas fa-check"></i></button>
                                      <button onClick={((e) => closeEditmaterial(e.currentTarget.parentNode.parentNode.parentNode, type.type_pd)
                                      )} style={{ borderRadius: "50px" }} type="button" className="btn btn-danger btn-sm px-3"><i className="fas fa-times"></i></button>
                                    </div>
                                  </td>
                                </tr>

                              ))}

                            </tbody>
                          </table>
                        </div>
                      </div>

                    </div>

                  </div>

                </div>
                <div className="tab-pane fade" id="v-pills-profile" role="tabpanel" aria-labelledby="v-pills-profile-tab">
                  <div className="table-responsive">
                    <table className="table product-table">
                      <caption>รายชื่อ ผู้ให้งาน</caption>
                      <thead className="">
                        <tr className="grey lighten-4" >
                          <th scope="col"></th>
                          <th scope="col">รหัสผู้ใช้</th>
                          <th scope="col">ชื่อ</th>
                          <th scope="col">สกุล</th>
                          <th scope="col">อีเมล์</th>
                        </tr>
                      </thead>
                      <tbody>
                        {datausers.map((user, i) => (
                          <tr key={i}>
                            <td scope="row" style={{ width: "10%" }}>
                              <div className="btn-group" role="group" aria-label="Basic example">
                                <a onClick={(() => modalsearchUser(user.id))} className="mr-2 text-info"><h5><i className="fas fa-search fa-flip-horizontal"></i></h5></a>
                                <a onClick={(() => modaleditUser(user.id))} className="mr-2 text-warning"><h5><i className="fas fa-pencil-alt"></i></h5></a>
                                <a onClick={(() => deleteUser(user.id))} className="text-danger"><h5><i className="fas fa-trash-alt"></i></h5></a>
                              </div>
                            </td>
                            <td>{user.username}</td>
                            <td>{user.firstname}</td>
                            <td>{user.lastname}</td>
                            <td>{user.email}</td>
                          </tr>
                        ))}

                      </tbody>
                    </table>
                  </div>
                </div>
                <div className="tab-pane fade" id="v-pills-ui" role="tabpanel" aria-labelledby="v-pills-ui-tab">
                  <div className="card">
                    <div className="card-header text-center">
                      <div className="border border-info rounded-pill p-3">
                        จัดการหน้าเว็บ
                      </div>
                    </div>
                    <form onSubmit={((e) => saveIndexui(e))} id="formindexui">
                      <div className="card-body">

                        <div className="md-form">
                          <input type="hidden" name="headerCheck" defaultValue={dataui.length ? dataui[0].text1 : ""} />
                          <textarea name="header" id="Header" className="form-control md-textarea " defaultValue={dataui.length ? dataui[0].text1 : ""} required />
                          <label htmlFor="Header" className="addclass">หัวข้อ</label>
                        </div>
                        <div className="md-form">
                          <input type="hidden" name="detailsHeadercheck" defaultValue={dataui.length ? dataui[0].text2 : ""} />
                          <textarea name="detailsHeader" id="DetailsHeader" className="form-control md-textarea " defaultValue={dataui.length ? dataui[0].text2 : ""} required />
                          <label htmlFor="DetailsHeader" className="addclass">รายละเอียด</label>
                        </div>
                        <div className="md-form">
                          <input type="hidden" name="detailsNewproductcheck" defaultValue={dataui.length ? dataui[0].text3 : ""} />
                          <textarea name="detailsNewproduct" id="DetailsNewproduct" className="form-control md-textarea " defaultValue={dataui.length ? dataui[0].text3 : ""} required />
                          <label htmlFor="DetailsNewproduct" className="addclass" >รายละเอียด สินค้าใหม่</label>
                        </div>

                      </div>
                      {btnsaveindexuitoggle ?
                        <div className="card-footer text-right">
                          <button style={{ borderRadius: "50px" }} type="submit" className="btn btn-outline-success">บันทึก</button>
                        </div>
                        : ""}
                    </form>
                  </div>
                </div>

                <div className="tab-pane fade" id="v-pills-messages" role="tabpanel" aria-labelledby="v-pills-messages-tab">

                  <div className="container h-100 py-2">
                    <ul className="nav nav-tabs border-0" id="myTab" role="tablist">
                      <li className="nav-item">
                        <a className="nav-link active border border-primary border-bottom-0" id="rate-tab" data-toggle="tab" href="#rate" role="tab" aria-controls="home" aria-selected="true">ประเมินราคา</a>
                      </li>
                      <li className="nav-item">
                        <a className="nav-link  border border-secondary border-bottom-0" id="home1-tab" data-toggle="tab" href="#tabcheck11" role="tab" aria-controls="home" aria-selected="true">ตรวจสอบการชำระ</a>
                      </li>
                      <li className="nav-item">
                        <a className="nav-link border border-danger border-bottom-0" id="profile1-tab" data-toggle="tab" href="#tabcheck21" role="tab" aria-controls="profile" aria-selected="false">ดำเนินการส่ง</a>
                      </li>
                      <li className="nav-item">
                        <a className="nav-link border border-warning border-bottom-0" id="messages1-tab" data-toggle="tab" href="#tabcheck31" role="tab" aria-controls="messages" aria-selected="false">ประวัติการสั่งซื้อ</a>
                      </li>
                    </ul>
                    <div className="tab-content h-75">
                      <div className="tab-pane h-100 p-3 active border border-primary radius box" id="rate" role="tabpanel" aria-labelledby="rate-tab">
                        <MDBTable hover>
                          <MDBTableHead>
                            <tr>
                              <th></th>
                              <th>เลขออเดอร์</th>
                              <th>วันที่สั่งซื้อ</th>
                              <th>จำนวนสินค้า</th>
                            </tr>
                          </MDBTableHead>
                          <MDBTableBody>
                            {dataAllpayment.map((o, i) => {
                              if (o.status == "1" && o.type_p == "2") {
                                return (
                                  <tr key={i}>
                                    <td>
                                      <span key="cell0" style={{ marginLeft: "20%", marginRight: "30%", display: "flex" }}>
                                        <a onClick={(() => assess_modal(o))} className="mr-2 text-info"><h5><i className="fas fa-search fa-flip-horizontal"></i></h5></a>
                                      </span>
                                    </td>
                                    <td>{o.order_id}</td>
                                    <td>{o.order_date}</td>
                                    <td>{o.allquantity} <span className="badge badge-danger ml-2">{o.read != '1' ? '' : 'New'}</span></td>
                                  </tr>
                                )
                              }
                            })}
                          </MDBTableBody>
                        </MDBTable>
                      </div>
                      <div className="tab-pane h-100 p-3  border border-secondary radius box" id="tabcheck11" role="tabpanel" aria-labelledby="home1-tab">
                        <MDBTable hover>
                          <MDBTableHead>
                            <tr>
                              <th></th>
                              <th>เลขออเดอร์</th>
                              <th>วันที่สั่งซื้อ</th>
                              <th>จำนวนสินค้า</th>
                              <th>ราคารวม</th>
                            </tr>
                          </MDBTableHead>
                          <MDBTableBody>
                            {dataAllpayment.map((o, i) => {
                              if (o.status == "3" && o.type_p == "2") {
                                return (
                                  <tr key={i}>
                                    <td>
                                      <span key="cell0" style={{ marginLeft: "20%", marginRight: "30%", display: "flex" }}>
                                        <a onClick={(() => check_payment(o))} className="mr-2 text-info"><h5><i className="fas fa-search fa-flip-horizontal"></i></h5></a>
                                      </span>
                                    </td>
                                    <td>{o.order_id}</td>
                                    <td>{o.order_date}</td>
                                    <td>{o.allquantity}</td>
                                    <td>{Number(o.allquantity * o.production_price).toLocaleString()}.00</td>
                                  </tr>
                                )
                              }
                            })}
                          </MDBTableBody>
                        </MDBTable>
                      </div>
                      <div className="tab-pane h-100 p-3 border border-danger radius2 box" id="tabcheck21" role="tabpanel" aria-labelledby="profile1-tab">
                        <MDBTable hover>
                          <MDBTableHead>
                            <tr>
                              <th></th>
                              <th>เลขออเดอร์</th>
                              <th>วันที่สั่งซื้อ</th>
                              <th>จำนวนสินค้า</th>
                              <th>ราคารวม</th>
                            </tr>
                          </MDBTableHead>
                          <MDBTableBody>
                            {dataAllpayment.map((o, i) => {
                              if (o.status == "4" && o.type_p == "2") {
                                return (
                                  <tr key={i}>
                                    <td>
                                      <span key="cell0" style={{ marginLeft: "20%", marginRight: "30%", display: "flex" }}>
                                        <a onClick={(() => view_payment(o))} className="mr-2 text-info"><h5><i className="fas fa-search fa-flip-horizontal"></i></h5></a>
                                      </span>
                                    </td>
                                    <td>{o.order_id}</td>
                                    <td>{o.order_date}</td>
                                    <td>{o.allquantity}</td>
                                    <td>{Number(o.allquantity * o.production_price).toLocaleString()}.00</td>
                                  </tr>
                                )
                              }
                            })}
                          </MDBTableBody>
                        </MDBTable>
                      </div>
                      <div className="tab-pane h-100 p-3 border border-warning radius2 box" id="tabcheck31" role="tabpanel" aria-labelledby="messages1-tab">
                        <form onSubmit={searchProduction}>
                          <div className="row">
                            <div className="col-5">
                              <div className="md-form">
                                <input type="date" name="firstdate" id="firstdate" className="form-control " />
                                <label htmlFor="firstdate">วันที่เริ่มต้น</label>
                              </div>
                            </div>
                            <div className="col-5">
                              <div className="md-form">
                                <input type="date" name="lastdate" id="lastdate" className="form-control " />
                                <label htmlFor="lastdate">วันที่สิ้นสุด</label>
                              </div>
                            </div>
                            <div className="col-2 col-md-2 d-flex justify-content-end">
                              <div className="md-form">
                                <div className="btn-group" role="group" aria-label="Basic example">
                                  <button type="submit" className="btn btn-info p-2">ค้นหา</button>
                                  <button type="button" onClick={()=>{
                                    var firstdate = document.getElementById("firstdate").value;
                                    var lastdate = document.getElementById("lastdate").value;
                                    report(dataAllpayment, firstdate, lastdate, "production");
                                    }} className="btn btn-info p-2">พิม</button>
                                </div>
                              </div>
                              {/* <div className="md-form">
                              <button type="submit" className="btn btn-info">ค้นหา</button>
                              <button type="submit" className="btn btn-info">ค้นหา</button>
                              </div> */}
                            </div>
                          </div>
                        </form>
                        <MDBTable hover>
                          <MDBTableHead>
                            <tr>
                              <th></th>
                              <th>เลขออเดอร์</th>
                              <th>วันที่จัดส่ง</th>
                              <th>จำนวนสินค้า</th>
                              <th>ราคารวม</th>
                            </tr>
                          </MDBTableHead>
                          <MDBTableBody>
                            {searchProdtionData.map((o, i) => {
                              if (o.status == "5" && o.type_p == "2") {
                                return (
                                  <tr key={i}>
                                    <td>
                                      <span key="cell0" style={{ marginLeft: "20%", marginRight: "30%", display: "flex" }}>
                                        <a onClick={(() => view_payment(o))} className="mr-2 text-info"><h5><i className="fas fa-search fa-flip-horizontal"></i></h5></a>
                                      </span>
                                    </td>
                                    <td>{o.order_id}</td>
                                    <td>{o.payment_date}</td>
                                    <td>{o.allquantity}</td>
                                    <td>{Number(o.allquantity * o.production_price).toLocaleString()}.00</td>
                                  </tr>
                                )
                              }
                            })}
                          </MDBTableBody>
                        </MDBTable>
                      </div>
                    </div>
                  </div>

                </div>
                <div className="tab-pane fade" id="v-pills-settings" role="tabpanel" aria-labelledby="v-pills-settings-tab">
                  <div className="container h-100 py-2">
                    <ul className="nav nav-tabs border-0" id="myTab" role="tablist">
                      <li className="nav-item">
                        <a className="nav-link active border border-primary border-bottom-0" id="home-tab" data-toggle="tab" href="#tabcheck1" role="tab" aria-controls="home" aria-selected="true">ตรวจสอบการชำระ</a>
                      </li>
                      <li className="nav-item">
                        <a className="nav-link border border-danger border-bottom-0" id="profile-tab" data-toggle="tab" href="#tabcheck2" role="tab" aria-controls="profile" aria-selected="false">ดำเนินการส่ง</a>
                      </li>
                      <li className="nav-item">
                        <a className="nav-link border border-warning border-bottom-0" id="messages-tab" data-toggle="tab" href="#tabcheck3" role="tab" aria-controls="messages" aria-selected="false">ประวัติการสั่งซื้อ</a>
                      </li>
                    </ul>
                    <div className="tab-content h-75">
                      <div className="tab-pane h-100 p-3 active border border-primary radius box" id="tabcheck1" role="tabpanel" aria-labelledby="home-tab">
                        <MDBTable hover>
                          <MDBTableHead>
                            <tr>
                              <th></th>
                              <th>เลขออเดอร์</th>
                              <th>วันที่สั่งซื้อ</th>
                              <th>จำนวนสินค้า</th>
                              <th>ราคารวม</th>
                            </tr>
                          </MDBTableHead>
                          <MDBTableBody>
                            {dataAllpayment.map((o, i) => {
                              if (o.status == "3" && o.type_p == "1") {
                                return (
                                  <tr key={i}>
                                    <td>
                                      <span style={{ marginLeft: "20%", marginRight: "30%", display: "flex" }}>
                                        <a onClick={(() => check_payment(o))} className="mr-2 text-info"><h5><i className="fas fa-search fa-flip-horizontal"></i></h5></a>
                                      </span>
                                    </td>
                                    <td>{o.order_id}</td>
                                    <td>{o.order_date}</td>
                                    <td>{o.allquantity}</td>
                                    <td>{Number(o.sumtotal).toLocaleString()}.00<span className="badge badge-danger ml-2">{o.read != '1' ? '' : 'New'}</span></td>
                                  </tr>
                                )
                              }
                            })}
                          </MDBTableBody>
                        </MDBTable>
                      </div>
                      <div className="tab-pane h-100 p-3 border border-danger radius2 box" id="tabcheck2" role="tabpanel" aria-labelledby="profile-tab">
                        <MDBTable hover>
                          <MDBTableHead>
                            <tr>
                              <th></th>
                              <th>เลขออเดอร์</th>
                              <th>วันที่สั่งซื้อ</th>
                              <th>จำนวนสินค้า</th>
                              <th>ราคารวม</th>
                            </tr>
                          </MDBTableHead>
                          <MDBTableBody>
                            {dataAllpayment.map((o, i) => {
                              if (o.status == "4" && o.type_p == "1") {
                                return (
                                  <tr key={i}>
                                    <td>
                                      <span key="cell0" style={{ marginLeft: "20%", marginRight: "30%", display: "flex" }}>
                                        <a onClick={(() => view_payment(o))} className="mr-2 text-info"><h5><i className="fas fa-search fa-flip-horizontal"></i></h5></a>
                                      </span>
                                    </td>
                                    <td>{o.order_id}</td>
                                    <td>{o.order_date}</td>
                                    <td>{o.allquantity}</td>
                                    <td>{Number(o.sumtotal).toLocaleString()}.00</td>
                                  </tr>
                                )
                              }
                            })}
                          </MDBTableBody>
                        </MDBTable>
                      </div>
                      <div className="tab-pane h-100 p-3 border border-warning radius2 box" id="tabcheck3" role="tabpanel" aria-labelledby="messages-tab">
                        <form onSubmit={searchBuy}>
                          <div className="row">
                            <div className="col-6 col-md-5">
                              <div className="md-form">
                                <input type="date" name="firstdate" id="firstdate2" className="form-control " />
                                <label htmlFor="firstdate">วันที่เริ่มต้น</label>
                              </div>
                            </div>
                            <div className="col-6 col-md-5">
                              <div className="md-form">
                                <input type="date" name="lastdate" id="lastdate2" className="form-control " />
                                <label htmlFor="lastdate">วันที่สิ้นสุด</label>
                              </div>
                            </div>
                            <div className="col-2 col-md-2 d-flex justify-content-end">
                              <div className="md-form">
                              <div className="btn-group" role="group" aria-label="Basic example">
                                <button type="submit" className="btn p-2 btn-info">ค้นหา</button>
                                <button type="button" onClick={()=>{
                                    var firstdate = document.getElementById("firstdate2").value;
                                    var lastdate = document.getElementById("lastdate2").value;
                                    report(dataAllpayment, firstdate, lastdate, "buy");
                                    }} className="btn btn-info p-2">พิม</button>
                                </div>
                              </div>
                            </div>
                          </div>
                        </form>
                        <MDBTable hover>
                          <MDBTableHead>
                            <tr>
                              <th></th>
                              <th>เลขออเดอร์</th>
                              <th>วันที่จัดส่ง</th>
                              <th>จำนวนสินค้า</th>
                              <th>ราคารวม</th>
                            </tr>
                          </MDBTableHead>
                          <MDBTableBody>
                            {searchBuyData.map((o, i) => {
                              if (o.status == "5" && o.type_p == "1") {
                                return (
                                  <tr key={i}>
                                    <td>
                                      <span key="cell0" style={{ marginLeft: "20%", marginRight: "30%", display: "flex" }}>
                                        <a onClick={(() => view_payment(o))} className="mr-2 text-info"><h5><i className="fas fa-search fa-flip-horizontal"></i></h5></a>
                                      </span>
                                    </td>
                                    <td>{o.order_id}</td>
                                    <td>{o.payment_date}</td>
                                    <td>{o.allquantity}</td>
                                    <td>{Number(o.sumtotal).toLocaleString()}.00</td>
                                  </tr>
                                )
                              }
                            })}
                          </MDBTableBody>
                        </MDBTable>
                      </div>
                    </div>
                  </div>

                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
      <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.4/umd/popper.min.js"></script>
      <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/js/bootstrap.min.js"></script>
      <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.8.11/js/mdb.min.js"></script>
    </div>
  );
}

export default management;
