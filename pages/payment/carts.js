import React, { useState, useEffect } from 'react';
import { useRouter } from 'next/router';
import Layout from '../../components/Layout';
import Swal from 'sweetalert2';
import withReactContent from 'sweetalert2-react-content';
import axios from 'axios';

const carts = () => {
  const MySwal = withReactContent(Swal);
  const router = useRouter()
  const [cartProduct, setCartproduct] = useState([]);
  const [address, setDefaultAddress] = useState(false);


  const getStocart = () => {
    var stoCart = JSON.parse(localStorage.getItem("cart"));
    setCartproduct(stoCart);

  }

  useEffect(() => {
    getStocart();
    jqueryCode();
  }, [])


  const jqueryCode = () => {
    $(document).ready(function () {
      $(document).on('change', '.qty', function () {

        var min = parseInt($(this).attr('min'));
        if ($(this).val() < min) {
          $(this).val(min);
        }

      });

      $(document).on('change', '.qty', function () {

        var qty = $(this).val();
        var price = $(this).siblings('.price').val();
        var id = $(this).siblings('.id').val();
        $(this).parent().parent().find('.amount').text(Number(qty * price).toLocaleString());
        $(this).parent().parent().find('.amountValue').val(qty * price);
        var stoCart = JSON.parse(localStorage.getItem("cart"));

        var objIndex = stoCart.findIndex((obj => obj.id == id));
        stoCart[objIndex].count = qty;
        localStorage.setItem("cart", JSON.stringify(stoCart));


      });

      $(document).on('input change', '.qty', function () {

        var totalSum = 0;
        $('.amountValue').each(function () {
          var inputValue = $(this).val();

          if ($.isNumeric(inputValue)) {
            totalSum += parseFloat(inputValue);
          }
        });
        $('#totalSum').text(Number(totalSum).toLocaleString());
      });
    });

  }

  const defaultAddress = (e) => {
    if (e.target.checked) {
      var address = localStorage.getItem("Address");
      var id = localStorage.getItem("Id");
      if (!id) {
        $('#fullHeightModalRight').modal('hide');
        setTimeout(function () {
          router.push('/login_register');
        }, 1000);
        // router.push('/login_register');
      } else if (!address) {
        if (confirm("Profiles not address")) {
          $('#fullHeightModalRight').modal('hide');
          router.push('/profile');
        }
      } else {
        document.getElementById('address').value = address;
        setDefaultAddress(true)
      }
    } else {
      document.getElementById('address').value = '';
      setDefaultAddress(false)
    }
  }

  const remove = (id) => {
    getStocart();
    var stoCart = cartProduct;
    var deleteCart = stoCart.filter(function (item) {
      return item.id !== id;
    })
    setCartproduct(deleteCart);
    localStorage.setItem("cart", JSON.stringify(deleteCart));
  }

  const sendOrder = (e) => {
    e.preventDefault();
    var userID = localStorage.getItem("Id");
    var stoCart = JSON.parse(localStorage.getItem("cart"));
    if (userID) {
      MySwal.fire({
        title: 'กำลังบันทึก สินค้า',
        showConfirmButton: false,
        onOpen: () => {
          MySwal.showLoading();
        }
      })
      var items = {
        "carts": stoCart,
        "id_user": userID,
        "address": e.target.address.value,
        "type_p": 1
      }

      axios.put(`${process.env.host_api}/payment`, items)
        .then(response => {
          console.log(response.data);
          $('#cart_address').modal('hide');
          localStorage.removeItem("cart");
          setTimeout(function () {
            MySwal.close();
            router.push({
              pathname: '/payment/payment_orders',
              query: { order_id: response.data.order_id, type_p: "1" },
            })
          }, 1000);

        })
        .catch(function (error) {
          console.log(error);
        });
    } else {
      if (confirm("Register to buy products")) {
        $('#fullHeightModalRight').modal('hide');
        router.push('/login_register');
      }

    }

  }

  return (
    <Layout
      page="index"
    >
      <form onSubmit={((e) => sendOrder(e))}>
        <div className="modal fade right" id="cart_address" tabIndex={-1} role="dialog" data-backdrop="false" aria-labelledby="myModalLabel" aria-hidden="true">
          <div className="modal-dialog modal-full-height modal-bottom" role="document">
            <div className="modal-content">
              <div className="modal-header">
                <h4 className="modal-title w-100" id="myModalLabel">ที่อยู่</h4>
                <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span>
                </button>
              </div>
              <div className="modal-body">
                <div className="form-group">
                  <label htmlFor="address">ที่อยู่ สำหรับจัดส่ง</label>
                  <textarea style={{ resize: "none" }} className="form-control border border-secondary" id="address" rows="3" disabled={address ? "disabled" : ""} required >

                  </textarea>
                </div>
                <div className="form-check">
                  <input type="checkbox" onChange={(e) => defaultAddress(e)} className="form-check-input" id="materialChecked2" checked={address ? true : false} />
                  <label className="form-check-label" htmlFor="materialChecked2">ใช้ที่อยู่เดิม</label>
                </div>
              </div>
              <div className="modal-footer">
                <button type="button" className="btn btn-secondary" data-dismiss="modal">ยกเลิก</button>
                <button type="submit" className="btn btn-primary">บันทึก</button>
              </div>
            </div>
          </div>
        </div>
      </form>
      
      <div className="card cart-style">
        <div className="card-body">
          {cartProduct && cartProduct.length ?
            <div className="table-responsive">
              <table className="table product-table">
                <thead className="mdb-color lighten-5">
                  <tr>
                    <th style={{ width: "300px" }} />
                    <th className="font-weight-bold">
                      <strong>ชื่อสินค้า</strong>
                    </th>
                    <th>
                      {/* <strong>ข้อความในป้าย</strong> */}
                    </th>
                    <th className="font-weight-bold">
                      <strong>ราคา</strong>
                    </th>
                    <th className="font-weight-bold">
                      <strong>จำนวน</strong>
                    </th>
                    <th className="font-weight-bold">
                      <strong>ราคารวม</strong>
                    </th>
                    <th className="close-th-size" />
                  </tr>
                </thead>
                <tbody>
                  {
                    cartProduct.map((prod, i) => (
                      <tr key={i} id="prod">
                        <td scope="row">
                          <img src={prod.img} className="img-fluid img-style z-depth-0" style={{maxHeight: "200px"}} />
                        </td>
                        <td>
                          <h5 className="mt-3" style={{
                            whiteSpace: "nowrap",
                            width: "120px",
                            overflow: "hidden",
                            textOverflow: "ellipsis"
                          }}>
                            <strong>{prod.name}</strong>
                          </h5>
                        </td>
                        <td >
                          {/* <input type="text" className="form-control" style={{ width: "110px" }} required /> */}

                        </td>
                        <td><a id="price">{Number(prod.price).toLocaleString()}</a>.00</td>
                        <td>
                          <input type="hidden" className="id" value={prod.id} />
                          <input type="hidden" className="price" value={prod.price} />
                          <input type="number" defaultValue={prod.count} min="1" className="form-control qty" style={{ width: 100 }} />
                        </td>
                        <td className="font-weight-bold">
                          <input type="hidden" className="amountValue" defaultValue={prod.price * prod.count} />
                          <a><strong className="amount">{Number(prod.price * prod.count).toLocaleString()}</strong>.00</a>
                        </td>
                        <td className="text-center">
                          <a onClick={() => remove(prod.id)} className="text-danger"><h3><i className="fas fa-trash-alt"></i></h3></a>
                        </td>
                      </tr>

                    ))}
                  <tr>
                    <td colSpan={3} />
                    <td>
                      <h4 className="mt-2">
                        <strong>ราคาทั้งหมด</strong>
                      </h4>
                    </td>
                    <td className="text-right">
                      <h4 className="mt-2">
                        <strong id="totalSum">{
                          cartProduct ? (cartProduct.map(o => o.price * o.count).reduce((a, c) => {
                            console.log(a);
                            
                            var sum = parseInt(a) + c;
                            return Number(sum);
                          }, 0).toLocaleString()
                          ) : ""}</strong><strong>.00 ฿</strong>
                      </h4>
                    </td>
                    <td colSpan={3} className="text-right">
                      <button type="button" className="btn btn-primary btn-rounded" data-toggle="modal" data-target="#cart_address">ถัดไป
                                            <i className="fas fa-angle-right right" />
                      </button>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
            :
            <div className="text-center">
              <i className="fas fa-shopping-cart fa-rotate-180 mr-2"></i>ไม่มีสินค้าในตะกร้า
            </div>
          }
        </div>
      </div>
      
      {/* <button className="click">กด</button> */}
      <style jsx>{`
      .cart-style{
        margin: 100px;
      }
      .img-style {
          display: block;
          margin-left: auto;
          margin-right: auto;
          width: 80%;
      }
      .close-th-size{
        width: 10%;
      }
      
      `}</style>

    </Layout>
  );
}

export default carts;
