import React, { useEffect, useState } from 'react';
import { useRouter } from 'next/router';
import axios from 'axios';
const invoice_order = () => {
  const router = useRouter();
  const [toggle, setToggle] = useState(false);
  const [data, setData] = useState([]);
  useEffect(() => {
    getOrder();
  }, [])

  const getOrder = () => {
    if (router.query.order_id) {
      axios.get(`${process.env.host_api}/payment/order_list/order_id/${router.query.order_id}`)
        .then(response => {
          setData(response.data);
          console.log(response.data);

        })
        .then(() => {

          setToggle(true);
        })
        .catch(function (error) {
          console.log(error);
        });
    } else {
      router.push('/payment/orders');
    }
  }


  const printDiv = () => {
    // var printContents = document.getElementById("print-invoice").innerHTML;
    // var originalContents = document.body.innerHTML;

    // document.body.innerHTML = printContents;

    window.print();

    // document.body.innerHTML = originalContents;

  }

  return (
    <div className="py-4 bg-img">
      <div id="button" style={{ display: "flex", justifyContent: "center" }}>
        <button id="btn" onClick={printDiv} type="button" className="btn btn-outline-warning" style={{ borderRadius: "25px" }} disabled={!toggle ? "disabled" : ""}> Print Invoice </button>
      </div>
      <div id="print-invoice" className="shadow-box mt-4 bg-white" style={{ height: "297mm", width: "210mm", margin: "auto", padding: "5mm" }}>
        {/* <div className="row" style={{ width: "100%",marginTop: "20px" }}> */}
        <div className="head">
          <span className="ml-2">
            <img src="../static/logo/icon-logo-invoice.png" style={{ height: "auto", width: "25mm" }} />
            <a style={{ marginLeft: "2mm", fontSize: "8mm", fontWeight: "bold" }}>SUPER SIGN</a>
          </span>
          <div className="address-comp mr-2">
            <strong>
              <p >
                201 ม.6 ต.ในคลองบางปลากด อ.พระสมุทรเจดีย์ จ.สมุทรปราการ 10290
                โทร 095-212-7146
              </p>
            </strong>
          </div>
        </div>
        <div className="margin-top">
          <p className="ml-2" style={{ color: "#07eee5", fontWeight: "bold" }}>INVOICE</p>
          <hr className="head-hr" />
          <div>
            <table className="ml-2">
              <tbody>
                <tr>
                  <td>
                    ใบแจ้งหนี้เลขที่
                </td>
                  <td width="20%"> </td>
                  <td>
                    {data.length ? router.query.order_id : ""}
                  </td>
                </tr>
                <tr>
                  <td>
                    วันที่
                </td>
                  <td></td>
                  <td>
                    {data.length ?
                      data[0].payment_date.split(',')[0]
                      : ""}
                  </td>
                </tr>
                <tr>
                  <td>
                    ราคารวม
                </td>
                  <td></td>
                  <td>
                    {data.reduce((a, b) => a + (b.price * b.quantity_payment), 0).toLocaleString()}
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
          <hr className="head-hr" />
          <table className="table table-striped mt-3">
            <thead>
              <tr>
                <th scope="col">#</th>
                <th scope="col" width="50%">ชื่อสินค้า</th>
                <th scope="col">ราคา/ชิ้น</th>
                <th scope="col">จำนวน</th>
                <th scope="col">ราคารวม</th>
              </tr>
            </thead>
            <tbody>
              {data.map((data, i) => (
                <tr key={i}>
                  <th scope="row">{i + 1}</th>
                  <td style={{ color: "#07eee5", fontWeight: "bold" }}>{data.name}</td>
                  <td align="right">{data.price}</td>
                  <td align="right">{data.quantity_payment}</td>
                  <td align="right">{data.price * data.quantity_payment}</td>
                </tr>
              ))}
            </tbody>
            <tfoot>
              <tr>
                <td colSpan="2">ขอบคุณสำหรับธุรกิจของคุณ</td>
                <td align="right">
                  <div>
                    ราคารวม
                  </div>
                  <div>
                    ภาษี
                  </div>
                  <div>
                  ราคาทั้งหมด
                  </div>
                </td>
                <td colSpan="2" align="right">
                  <div>
                    {data.reduce((a, b) => a + (b.price * b.quantity_payment), 0).toLocaleString()}
                  </div>
                  <div>
                    0%
                  </div>
                  <div>
                    {data.reduce((a, b) => a + (b.price * b.quantity_payment), 0).toLocaleString()}
                  </div>
                </td>
              </tr>
            </tfoot>
          </table>
        </div>
      </div>

      <style jsx>{`
      body{
        font-family: tahoma;
      }
      #btn:hover {
        background-color: white !important;
      }
      p{
        margin-bottom: 0;
      }
      hr{
        margin: 0;
      }
      .head-hr{
        border-top: 0.5mm solid #b4b4b4;
      }
      .margin-top{
        margin-top: 20mm;
      }
      .head{
        margin-top: 5mm;
        display: block;
      }
      .address-comp{
        float: right;
        width: 50mm;
      }
      .bg-img{
        background-image: url("../static/images/bg-invoice.png");
        background-color: #F8F8FF;
        height: 100%; 

        background-position: center;
        background-repeat: no-repeat;
        background-size: cover;
      }
      @media print {
        body * {
          visibility: hidden;
        }
        #button *{
          visibility: hidden;
        }
        #print-invoice, #print-invoice * {
          visibility: visible;
        }
        #print-invoice {
          position: absolute;
          left: 0;
          top: 0;
          right: 0;
          bottom: 0;
          width: unset !important;
          height: unset !important;
          box-shadow: none;
          margin: 0 !important;
        }
      }
      
      .shadow-box{
        box-shadow: 0 0 8px #bbb;
        border-radius: 4px;
      }
      `}</style>
    </div>
  );
}

export default invoice_order;
