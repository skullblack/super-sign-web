import React, { useState, useEffect } from 'react';
import Layout from '../../components/Layout';
import { useRouter } from 'next/router';
import Swal from 'sweetalert2';
import withReactContent from 'sweetalert2-react-content';
import axios from 'axios';

const payment_production = () => {
  const MySwal = withReactContent(Swal);
  const router = useRouter()
  const [typeProduct, setTypeproduct] = useState([]);
  const [address, setDefaultAddress] = useState(false);


  useEffect(() => {
    if(!localStorage.getItem("Id")){
      router.push('/login_register');
    }else{
      gettypeProduct();
    }
  }, [])

  const gettypeProduct = () => {
    axios.get(`${process.env.host_api}/storage/type`, {
      // http://ss-api.inventage.co
      headers: {
        Username: localStorage.getItem("Username"),
        Password: localStorage.getItem("Password")
      }

    })
      .then(response => {

        setTypeproduct(response.data);

      })
      .catch(function (error) {
        console.log(error);

      });
  }

  const saveProduction = (e) => {
    MySwal.fire({
      title: 'Save production order',
      showConfirmButton: false,
      onOpen: () => {
        MySwal.showLoading();
      }
    })
    e.preventDefault();
    var userID = localStorage.getItem("Id");

    if (userID) {
      var File = document.getElementById("img-pro").files;

      const formData = new FormData();
      formData.append("user_id", userID);
      formData.append("file", File[0]);
      formData.append("width", e.target.width.value);
      formData.append("height", e.target.height.value);
      formData.append("quantity", e.target.quantity.value);
      formData.append("materialtype", e.target.category.value);
      formData.append("detail", e.target.details.value);
      formData.append("address", e.target.address.value);
      formData.append("type_p", "2");


      axios.put(`${process.env.host_api}/payment`, formData,{
        headers: {
          'Content-Type': "multipart/form-data"
        }
      })
        .then(response => {
          MySwal.fire({
            icon: 'success',
            title: 'Finished!',
            timer: 1500,
            showConfirmButton: false
          }).then(() => {
            router.push('/payment/production_order');
          })
          // console.log(response.data);
        })
        .catch(function (error) {
          MySwal.fire({
            icon: 'error',
            title: 'error!'
          })
          console.log(error);
        });
    } else {
      if (confirm("Register to buy products")) {
        router.push('/login_register');
      }
    }
  }
  const getFilename = (e) => {
    document.getElementById("label-filename").innerText = e.target.files[0].name;
  }

  const defaultAddress = (e) => {
    if (e.target.checked) {
      var address = localStorage.getItem("Address");
      var id = localStorage.getItem("Id");
      if (!id) {
        // $('#fullHeightModalRight').modal('hide');
        setTimeout(function () {
          router.push('/login_register');
        }, 1000);
        // router.push('/login_register');
      } else if (!address) {
        if (confirm("Profiles not address")) {
          // $('#fullHeightModalRight').modal('hide');
          router.push('/profile');
        }
      } else {
        document.getElementById('address').value = address;
        setDefaultAddress(true)
      }
    } else {
      document.getElementById('address').value = '';
      setDefaultAddress(false)
    }
  }
  return (
    <Layout>
      <div className="container cart-style pt-4">
        <div className="card mb-4">
          <form onSubmit={((e) => saveProduction(e))}>
            <div className="card-body">
              <div className="custom-file">
                <input type="file" className="custom-file-input" onChange={((e) => getFilename(e))} id="img-pro" required />
                <label className="custom-file-label" id="label-filename" >รูปภาพ</label>
              </div>
              <div className="row">
                <div className="col">
                  <div className="md-form form-group">
                    <input type="number" className="form-control" min={1} pattern="[0-9]" id="width" required />
                    <label htmlFor="width">ความกว้าง (ซม.)</label>
                  </div>
                </div>
                <div className="col">
                  <div className="md-form form-group">
                    <input type="number" className="form-control" min={1} pattern="[0-9]" id="height" required />
                    <label htmlFor="height">ความสูง (ซม.)</label>
                  </div>
                </div>
              </div>
              <div className="row">
                <div className="col">
                  <div className="md-form form-group">
                    <input type="number" id="quantity" className="form-control " min="1" required />
                    <label htmlFor="quantity">จำนวนสังผลิต</label>
                  </div>
                </div>
                <div className="col">
                  <div className="md-form form-group">
                    <select className="form-control custom-select border-top-0 border-left-0 border-right-0" id="category" required>
                      <option value="">ประเภทวัสดุ</option>
                      {typeProduct.map((type, i) => (
                        <option key={i} value={type.id}>{type.type_pd}</option>
                      ))}
                    </select>
                  </div>
                </div>
              </div>
              <div className="md-form">
                <textarea id="details" className="form-control md-textarea " required />
                <label htmlFor="details">รายละเอียด</label>
              </div>
              <div className="md-form">
                <textarea id="address" className="form-control md-textarea " disabled={address} required />
                <label htmlFor="address" className={address? 'active':''}>ที่อยู่ในการจัดส่ง</label>
              </div>
              <div className="form-check">
                  <input type="checkbox" onChange={(e) => defaultAddress(e)} className="form-check-input" id="materialChecked2" checked={address ? true : false} />
                  <label className="form-check-label" htmlFor="materialChecked2">ใช้ที่อยู่เดิม</label>
                </div>
            </div>
            <div className="card-footer text-right">
              <button type="submit" className="btn btn-outline-success waves-effect" >บันทึก</button>
            </div>
          </form>
        </div>
      </div>
      <style jsx>{`
      .cart-style{
        margin-top: 65px;
      }
      .img-style {
          display: block;
          margin-left: auto;
          margin-right: auto;
          width: 80%;
      }
      
      `}</style>
    </Layout>
  );
}

export default payment_production;
