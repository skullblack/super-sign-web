import React, { useState, useEffect } from 'react';
import { useRouter } from 'next/router';
import Layout from '../../components/Layout';
import Swal from 'sweetalert2';
import withReactContent from 'sweetalert2-react-content';
import axios from 'axios';


const view_payment = () => {
  const MySwal = withReactContent(Swal);
  const router = useRouter()
  const [orders, setOrders] = useState([]);
  useEffect(() => {
    getorders();
  }, [])
  const getorders = () => {
    MySwal.fire({
      title: 'กำลังโหลดข้อมูล',
      showConfirmButton: false,
      onOpen: () => {
        MySwal.showLoading();
      }
    })
    if (router.query.order_id && router.query.status && router.query.type_p) {
      if (router.query.type_p == "1") {

        axios.get(`${process.env.host_api}/payment/view_payment/order_id/${router.query.order_id}`)
          .then(response => {
            MySwal.close();
            setOrders(response.data)
          })
          .catch(function (error) {
            Swal.fire({
              icon: 'error',
              title: 'บันทึกผิดพลาด',
            })
            console.log(error);
          });
      } else {
        console.log("2");
        axios.get(`${process.env.host_api}/payment/view_payment_production/order_id/${router.query.order_id}`)
          .then(response => {
            MySwal.close();
            setOrders(response.data)
          })
          .catch(function (error) {
            Swal.fire({
              icon: 'error',
              title: 'บันทึกผิดพลาด',
            })
            console.log(error);
          });
      }


    } else {
      router.push('/management');
    }
  }

  const up_status = () => {
    MySwal.fire({
      title: 'กำลังบันทึก',
      showConfirmButton: false,
      onOpen: () => {
        MySwal.showLoading();
      }
    })
    axios.post(`${process.env.host_api}/payment/up_status5`, {
      order_id: router.query.order_id
    })
      .then(() => {
        Swal.fire({
          icon: 'success',
          title: 'บันทึกสำเร็จ',
          showConfirmButton: false,
          timer: 1500
        }).then(() => {
          router.push('/management');
          MySwal.close();
        })
      })
      .catch(function (error) {
        Swal.fire({
          icon: 'error',
          title: 'บันทึกผิดพลาด',
        })
        console.log(error);
      });
  }


  return (
    <Layout>

      <div className="container">
        <div className="card cart-style">
          <div className="card-body">
            {router.query.type_p == "1" ?
              <div className="table-responsive">
                <table className="table product-table">
                  <thead className="mdb-color lighten-5">
                    <tr>
                      <th style={{ width: "300px" }} />
                      <th className="font-weight-bold">
                        <strong>ชื่อสินค้า</strong>
                      </th>
                      <th />
                      <th className="font-weight-bold">
                        <strong>ราคาสินค้า</strong>
                      </th>
                      <th className="font-weight-bold">
                        <strong>จำนวนสินค้า</strong>
                      </th>
                      <th className="font-weight-bold">
                        <strong>ราคารวม</strong>
                      </th>
                    </tr>
                  </thead>
                  <tbody>
                    {orders.map((prod, i) => (
                      <tr key={i} >
                        <th scope="row">
                          <img src={prod.picture} className="img-fluid img-style z-depth-0" />
                        </th>
                        <td>
                          <h5 className="mt-3">
                            <strong>{prod.name}</strong>
                          </h5>
                        </td>
                        <td />
                        <td>
                          <a id="price">{prod.price}</a>
                        </td>
                        <td>
                          <strong>{prod.quantity_payment}</strong>
                        </td>
                        <td className="font-weight-bold">
                          <strong className="amount">{(prod.price * prod.quantity_payment).toLocaleString()}</strong>
                        </td>
                      </tr>
                    ))}
                    <tr>
                      <td colSpan={3} />
                      <td>
                        <h4 className="mt-2">
                          <strong>Total</strong>
                        </h4>
                      </td>
                      <td colSpan={2} className="text-right">
                        <h4 className="mt-2">
                          <strong id="totalSum">{orders.reduce((a, b) => a + (b.price * b.quantity_payment), 0).toLocaleString()}</strong><strong > ฿</strong>
                        </h4>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
              :
              <div className="table-responsive">
                <table className="table product-table">
                  <thead className="mdb-color lighten-5">
                    <tr>
                      <th style={{ width: "300px" }} />
                      <th />
                      <th className="font-weight-bold">
                        <strong>ราคาสินค้า</strong>
                      </th>
                      <th className="font-weight-bold">
                        <strong>ความกว้าง</strong>
                      </th>
                      <th className="font-weight-bold">
                        <strong>ความสูง</strong>
                      </th>
                      <th className="font-weight-bold">
                        <strong>จำนวนสินค้า</strong>
                      </th>
                    </tr>
                  </thead>
                  <tbody>
                    {orders.map((prod, i) => (
                      <tr key={i} >
                        <th scope="row">
                          <img src={prod.design_picture} className="img-fluid img-style z-depth-0" />
                        </th>
                        <td />
                        <td>
                          <a id="price">{(prod.production_price).toLocaleString()}</a>
                        </td>
                        <td>
                          <a >{prod.width}</a>
                        </td>
                        <td>
                          <a >{prod.height}</a>
                        </td>
                        <td>
                          <strong>{prod.quantity_payment}</strong>
                        </td>
                      </tr>
                    ))}
                    <tr>
                      <td colSpan={3} />
                      <td>
                        <h4 className="mt-2">
                          <strong>Total</strong>
                        </h4>
                      </td>
                      <td colSpan={2} className="text-right">
                        <h4 className="mt-2">
                          <strong >{orders.reduce((a, b) => a + (b.production_price * b.quantity_payment), 0).toLocaleString()}</strong><strong > ฿</strong>
                        </h4>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
            }
          </div>
        </div>
        <div className="card my-5">
          <div className="card-body">
            <img src={orders[0] ? orders[0].premise : ""} className="img-fluid img-style z-depth-0" />
          </div>
        </div>
        <div className="card my-5">
          <div className="card-body">
            {/* ข้อมูลลูกค้า */}
            {orders.length ? (
              <div className="grey lighten-4 m-2 p-3" >
                <div className="form-group row">
                  <label htmlFor="searchUsername" className="col-sm-2 col-form-label">รหัสผู้ใช้</label>
                  <div className="col-sm-10">
                    <input type="text" className="form-control border-0" id="searchUsername" value={orders[0].username} disabled />
                  </div>
                </div>
                <div className="form-group row">
                  <label htmlFor="searchFirstname" className="col-sm-2 col-form-label">ชื่อ</label>
                  <div className="col-sm-10">
                    <input type="text" className="form-control border-0" id="searchFirstname" value={orders[0].firstname} disabled />
                  </div>
                </div>
                <div className="form-group row">
                  <label htmlFor="searchLastname" className="col-sm-2 col-form-label">สกุล</label>
                  <div className="col-sm-10">
                    <input type="text" className="form-control border-0" id="searchLastname" value={orders[0].lastname} disabled />
                  </div>
                </div>
                <div className="form-group row">
                  <label htmlFor="searchEmail" className="col-sm-2 col-form-label">อีเมลล์</label>
                  <div className="col-sm-10">
                    <input type="text" className="form-control border-0" id="searchEmail" value={orders[0].email} disabled />
                  </div>
                </div>
                <div className="form-group row">
                  <label htmlFor="searchTel" className="col-sm-2 col-form-label">เบอร์โทร</label>
                  <div className="col-sm-10">
                    <input type="text" className="form-control border-0" id="searchTel" value={orders[0].tel} disabled />
                  </div>
                </div>
                <div className="form-group row">
                  <label htmlFor="searchAddress" className="col-sm-2 col-form-label">ที่อยู่</label>
                  <div className="col-sm-10">
                    <textarea id="searchAddress" className="form-control md-textarea noresize" value={orders[0].user_address} disabled />
                  </div>
                </div>
              </div>
            ) : ""}
          </div>
          <div className="card-footer text-right">
            {router.query.status == "4" ? (
              <div>
                <button onClick={(() => router.push('/management'))} type="button" className="btn btn-outline-warning waves-effect" disabled={!orders.length ? "disabled" : ""}>กลับ</button>
                <button onClick={up_status} type="button" className="btn btn-outline-success waves-effect" disabled={!orders.length ? "disabled" : ""}>จัดส่งแล้ว</button>
              </div>
            ) : (
                <button onClick={(() => router.push('/management'))} type="button" className="btn btn-outline-warning waves-effect" disabled={!orders.length ? "disabled" : ""}>กลับ</button>
              )}
          </div>
        </div>
      </div>
      <style jsx>{`
      .cart-style{
        margin-top: 10%;
      }
      .img-style {
          display: block;
          margin-left: auto;
          margin-right: auto;
          width: 80%;
      }
      .noresize {
        resize: none;
        height: 150px; 
      }
      
      `}</style>
    </Layout>
  );
}

export default view_payment;
