import React, { useState, useEffect } from 'react';
import { useRouter } from 'next/router';
import Layout from '../../components/Layout';
import Swal from 'sweetalert2';
import withReactContent from 'sweetalert2-react-content';
import axios from 'axios';


const check_payment = () => {
  const MySwal = withReactContent(Swal);
  const router = useRouter()
  const [orders, setOrders] = useState([]);
  useEffect(() => {
    getorders();
  }, [])
  const getorders = () => {
    MySwal.fire({
      title: 'กำลังโหลดข้อมูล',
      showConfirmButton: false,
      onOpen: () => {
        MySwal.showLoading();
      }
    })
    if (router.query.order_id && router.query.type_p) {
      if (router.query.type_p == "1") {
        axios.get(`${process.env.host_api}/payment/check_status3/order_id/${router.query.order_id}`)
          .then(response => {
            console.log(response.data);
          MySwal.close();
            setOrders(response.data)
          })
          .catch(function (error) {
            Swal.fire({
              icon: 'error',
              title: 'บันทึกผิดพลาด',
            })
            console.log(error);
          });
      } else {
        axios.get(`${process.env.host_api}/payment/check_status3_production/order_id/${router.query.order_id}`)
          .then(response => {
            console.log(response.data);
            MySwal.close();
            setOrders(response.data)
          })
          .catch(function (error) {
            Swal.fire({
              icon: 'error',
              title: 'บันทึกผิดพลาด',
            })
            console.log(error);
          });
      }

    } else {
      router.push('/management');
    }
  }
  const savedata = () => {
    MySwal.fire({
      title: 'กำลังบันทึก',
      showConfirmButton: false,
      onOpen: () => {
        MySwal.showLoading();
      }
    })
    axios.post(`${process.env.host_api}/payment/up_status4`, {
      order_id: router.query.order_id
    })
      .then(response => {
        Swal.fire({
          icon: 'success',
          title: 'บันทึกสำเร็จ',
          showConfirmButton: false,
          timer: 1500
        }).then(() => {
          router.push('/management');
          MySwal.close();
        })
        console.log(response.data);

      })
      .catch(function (error) {
        Swal.fire({
          icon: 'error',
          title: 'บันทึกผิดพลาด',
        })
        console.log(error);
      });
  }

  return (
    <Layout>

      <div className="container">
        <div className="card cart-style">
          <div className="card-body">
            {router.query.type_p == "1" ?
              <div className="table-responsive">
                <table className="table product-table">
                  <thead className="mdb-color lighten-5">
                    <tr>
                      <th style={{ width: "300px" }} />
                      <th className="font-weight-bold">
                        <strong>สินค้า</strong>
                      </th>
                      <th />
                      <th className="font-weight-bold">
                        <strong>ราคา</strong>
                      </th>
                      <th className="font-weight-bold">
                        <strong>จำนวน</strong>
                      </th>
                      <th className="font-weight-bold">
                        <strong>ราคารวม</strong>
                      </th>
                    </tr>
                  </thead>
                  <tbody>
                    {orders.map((prod, i) => (
                      <tr key={i} >
                        <th scope="row">
                          <img src={prod.picture} className="img-fluid img-style z-depth-0" />
                        </th>
                        <td>
                          <h5 className="mt-3">
                            <strong>{prod.name}</strong>
                          </h5>
                        </td>
                        <td />
                        <td>
                          <a id="price">{prod.price}.00</a>
                        </td>
                        <td>
                          <strong>{prod.quantity_payment}</strong>
                        </td>
                        <td className="font-weight-bold">
                          <strong className="amount">{(prod.price * prod.quantity_payment).toLocaleString()}.00</strong>
                        </td>
                      </tr>
                    ))}
                    <tr>
                      <td colSpan={3} />
                      <td>
                        <h4 className="mt-2">
                          <strong>ราคาทั้งหมด</strong>
                        </h4>
                      </td>
                      <td colSpan={2} className="text-right">
                        <h4 className="mt-2">
                          <strong id="totalSum">{orders.reduce((a, b) => a + (b.price * b.quantity_payment), 0).toLocaleString()}</strong><strong>.00 ฿</strong>
                        </h4>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
              :
              <div className="table-responsive">
                <table className="table product-table">
                  <thead className="mdb-color lighten-5">
                    <tr>
                      <th style={{ width: "300px" }} />
                      <th />
                      <th className="font-weight-bold">
                        <strong>ราคา</strong>
                      </th>
                      <th className="font-weight-bold">
                        <strong>ความกว้าง</strong>
                      </th>
                      <th className="font-weight-bold">
                        <strong>ความสูง</strong>
                      </th>
                      <th className="font-weight-bold">
                        <strong>จำนวน</strong>
                      </th>
                    </tr>
                  </thead>
                  <tbody>
                    {orders.map((prod, i) => (
                      <tr key={i} >
                        <th scope="row">
                          <img src={prod.design_picture} className="img-fluid img-style z-depth-0" />
                        </th>
                        <td />
                        <td>
                          <a id="price">{prod.production_price}.00</a>
                        </td>
                        <td>
                          <a >{prod.width}</a>
                        </td>
                        <td>
                          <a >{prod.height}</a>
                        </td>
                        <td>
                          <strong>{prod.quantity_payment}</strong>
                        </td>
                      </tr>
                    ))}
                    <tr>
                      <td colSpan={3} />
                      <td>
                        <h4 className="mt-2">
                          <strong>ราคาทั้งหมด</strong>
                        </h4>
                      </td>
                      <td colSpan={2} className="text-right">
                        <h4 className="mt-2">
                          <strong >{orders.reduce((a, b) => a + (b.production_price * b.quantity_payment), 0).toLocaleString()}</strong><strong>.00 ฿</strong>
                        </h4>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
            }
          </div>
        </div>
        <div className="card my-5">
          <div className="card-body">
            <img src={orders[0] ? orders[0].premise : ""} className="img-fluid img-style z-depth-0" />
          </div>
          <div className="card-footer text-right">
            <button onClick={(() => savedata())} type="button" className="btn btn-outline-success waves-effect" disabled={!orders.length ? "disabled" : ""}>ตกลง</button>
          </div>
        </div>
      </div>
      <style jsx>{`
      .cart-style{
        margin-top: 10%;
      }
      .img-style {
          display: block;
          margin-left: auto;
          margin-right: auto;
          width: 80%;
      }
      
      `}</style>
    </Layout>
  );
}

export default check_payment;
