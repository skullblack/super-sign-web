import React, { useState, useEffect } from 'react';
import Layout from '../../components/Layout';
import { useRouter } from 'next/router';
import Swal from 'sweetalert2';
import withReactContent from 'sweetalert2-react-content';
import axios from 'axios';
import { MDBTable, MDBTableBody, MDBTableHead } from 'mdbreact';

const orders = () => {
  const MySwal = withReactContent(Swal);
  const router = useRouter();
  const [searchToggle, setSearchtoggle] = useState(false);
  const [orders, setOrders] = useState([]);
  const [searchmodal, setSearchmodal] = useState([]);
  useEffect(() => {
    getorders();
  }, [])

  const getorders = () => {
    var iduser = localStorage.getItem("Id")
    axios.get(`${process.env.host_api}/payment/order_list/user_id/${iduser}`)
      .then(response => {
        setOrders(response.data)
        console.log(response.data);
        
        setTimeout(() => {
          MySwal.close();
        }, 500);

      })
      .catch(function (error) {
        console.log(error);
      });
  }

  const delete_pay = (e) => {
    MySwal.fire({
      title: 'กำลังลบ สินค้า',
      showConfirmButton: false,
      onOpen: () => {
        MySwal.showLoading();
      }
    })
    axios.post(`${process.env.host_api}/payment/delete/`, {
      order_id: e.order_id
    })
      .then(() => {
        getorders();
      })
      .catch(function (error) {
        console.log(error);
      });

  }

  const pay = (e) => {
    console.log(e);

    router.push({
      pathname: '/payment/payment_orders',
      query: { order_id: e.order_id, type_p: e.type_p },
    })

  }

  const searchModal = (e) => {
    setSearchtoggle(true);
    $("#searchModal").modal();
    axios.get(`${process.env.host_api}/payment/order_list/order_id/${e.order_id}`)
      .then(response => {

        setSearchmodal(response.data)
      })
      .then(() => {
        setSearchtoggle(false);
      })
      .catch(function (error) {
        console.log(error);
      });

  }
  const invoice = (obj) => {
    router.push({
      pathname: '/payment/invoice_order',
      query: { order_id: obj.order_id },
    })
  }

  return (
    <Layout>
      <div className="modal fade " id="searchModal" tabIndex={-1} role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div className="modal-dialog modal-lg" role="document">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title" id="exampleModalLongTitle">สินค้า</h5>
              <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
              </button>
            </div>
            <div className="modal-body">
              {searchToggle ?
                <div className="text-center">
                  <div className="spinner-border text-primary " role="status" />
                </div>
                :
                <div className="table-responsive">
                  <table className="table product-table">
                    <thead className="mdb-color lighten-5">
                      <tr>
                        <th style={{ width: "300px" }} />
                        <th className="font-weight-bold">
                          <strong>ชื่อสินค้า</strong>
                        </th>
                        <th />
                        <th className="font-weight-bold">
                          <strong>ราคา</strong>
                        </th>
                        <th className="font-weight-bold">
                          <strong>จำนวน</strong>
                        </th>
                        <th className="font-weight-bold">
                          <strong>ราคารวม</strong>
                        </th>
                      </tr>
                    </thead>
                    <tbody>
                      {searchmodal.map((prod, i) => (
                        <tr key={i} >
                          <th scope="row">
                            <img src={prod.picture} className="img-fluid img-style z-depth-0" />
                          </th>
                          <td>
                            <h5 className="mt-3">
                              <strong>{prod.name}</strong>
                            </h5>
                          </td>
                          <td />
                          <td>
                            <a id="price">{prod.price}.00</a>
                          </td>
                          <td>
                            <strong>{prod.quantity_payment}</strong>
                          </td>
                          <td className="font-weight-bold">
                            <strong className="amount">{(prod.price * prod.quantity_payment).toLocaleString()}.00</strong>
                          </td>
                        </tr>
                      ))}
                      <tr>
                        <td colSpan={3} />
                        <td>
                          <h4 className="mt-2">
                            <strong>ราคาทั้งหมด</strong>
                          </h4>
                        </td>
                        <td colSpan={2} className="text-right">
                          <h4 className="mt-2">
                            <strong id="totalSum">{searchmodal.reduce((a, b) => a + (b.price * b.quantity_payment), 0).toLocaleString()}.00</strong><strong > ฿</strong>
                          </h4>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>

              }

            </div>
          </div>
        </div>
      </div>

      <div className="payment-style f1">
        <div className="container h-100 py-2">
          <ul className="nav nav-tabs border-0" id="myTab" role="tablist">
            <li className="nav-item">
              <a className="nav-link active border border-primary border-bottom-0" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">ค้างชำระ</a>
            </li>
            <li className="nav-item">
              <a className="nav-link border border-warning border-bottom-0" id="messages-tab" data-toggle="tab" href="#messages" role="tab" aria-controls="messages" aria-selected="false">ประวัติการสั่งซื้อ</a>
            </li>
          </ul>
          <div className="tab-content h-75">
            <div className="tab-pane h-100 p-3 active border border-primary radius box" id="home" role="tabpanel" aria-labelledby="home-tab">
              <MDBTable hover>
                <MDBTableHead>
                  <tr>
                    <th>เลขออเดอร์</th>
                    <th>วันที่สั่งซื้อ</th>
                    <th>จำนวนสินค้า</th>
                    <th>ราคาสินค้ารวม</th>
                    <th>สถานะ</th>
                    <th></th>
                  </tr>
                </MDBTableHead>
                <MDBTableBody>
                  {orders.map((o, i) => {
                    if (o.status == "2" && o.type_p == "1") {
                      return (
                        <tr key={i}>
                          <td>{o.order_id}</td>
                          <td>{o.order_date}</td>
                          <td>{o.allquantity}</td>
                          <td>{Number(o.sumtotal).toLocaleString()}.00</td>
                          <td>รอการชำระ</td>
                          <td>
                            <span key="cell0" style={{ marginLeft: "20%", marginRight: "30%", display: "flex" }}>
                              <i onClick={(() => pay(o))} style={{ cursor: "pointer" }} className="far fa-credit-card text-success mr-3"></i>
                              <i onClick={(() => delete_pay(o))} style={{ cursor: "pointer" }} className="fas fa-trash-alt text-danger"></i>
                            </span>
                          </td>
                        </tr>
                      )
                    } else if ((o.status == "3" || o.status == "4") && o.type_p == "1") {
                      return (
                        <tr key={i}>
                          <td>{o.order_id}</td>
                          <td>{o.order_date}</td>
                          <td>{o.allquantity}</td>
                          <td>{Number(o.sumtotal).toLocaleString()}.00</td>
                          <td>{o.status == "3" ? "รอตรวจสอบ" : "กำลังจัดส่ง"}</td>
                          <td>
                            <span key="cell0" style={{ marginLeft: "20%", marginRight: "30%", display: "flex" }}>
                              <a onClick={(() => searchModal(o))} className="mr-2 text-info"><i className="fas fa-search fa-flip-horizontal"></i></a>
                            </span>
                          </td>
                        </tr>
                      )
                    }
                  })}
                </MDBTableBody>
              </MDBTable>
            </div>
            <div className="tab-pane h-100 p-3 border border-warning radius2" id="messages" role="tabpanel" aria-labelledby="messages-tab">
              <MDBTable hover>
                <MDBTableHead>
                  <tr>
                    <th></th>
                    <th>เลขออเดอร์</th>
                    <th>วันที่จัดส่ง</th>
                    <th>จำนวนสินค้า</th>
                    <th>ราคาสินค้ารวม</th>
                    <th>สถานะ</th>
                  </tr>
                </MDBTableHead>
                <MDBTableBody>
                  {orders.map((o, i) => {
                    if (o.status == "5" && o.type_p == "1") {
                      return (
                        <tr key={i}>
                          <td>
                            <span key="cell0" style={{ marginLeft: "20%", marginRight: "30%", display: "flex" }}>
                              <a onClick={(() => searchModal(o))} className="mr-2 text-info"><i className="fas fa-search fa-flip-horizontal"></i></a>
                              <a onClick={(() => invoice(o))} className="mr-2 text-info"><i className="fas fa-file-invoice ml-2"></i></a>
                            </span>
                          </td>
                          <td>{o.order_id}</td>
                          <td>{o.payment_date}</td>
                          <td>{o.allquantity}</td>
                          <td>{Number(o.sumtotal).toLocaleString()}.00</td>
                          <td>{o.status == "5" ? "จัดส่งแล้ว" : ""}</td>
                        </tr>
                      )
                    }
                  })}
                </MDBTableBody>
              </MDBTable>
            </div>
          </div>
        </div>

      </div>


      <style jsx>{`
        .payment-style{
            margin: 10%;
        }
        .nav-tabs .nav-link:not(.active) {
            border-color: transparent !important;
            border-radius: 15px;
        }
        .radius{
            border-radius: 0 10px 10px 10px; 
        }
        .radius2{
            border-radius: 10px; 
        }
        .box{
            overflow : auto;
        }

      
      `}</style>

    </Layout>
  );
}

export default orders;
