import React, { useState, useEffect } from 'react';
import { useRouter } from 'next/router';
import Layout from '../../components/Layout';
import Swal from 'sweetalert2';
import withReactContent from 'sweetalert2-react-content';
import axios from 'axios';


const payment_orders = () => {
  const MySwal = withReactContent(Swal);
  const router = useRouter()
  const [orders, setOrders] = useState([]);
  useEffect(() => {
    getorders();
    
    
  }, [])
  const getorders = () => {
    if (router.query.order_id && router.query.type_p) {
      if (router.query.type_p == "1") {
        axios.get(`${process.env.host_api}/payment/order_list/order_id/${router.query.order_id}`)
          .then(response => {
            
            setOrders(response.data)
          })
          .catch(function (error) {
            console.log(error);
          });
      } else {
        axios.get(`${process.env.host_api}/payment/order_list_production/order_id/${router.query.order_id}`)
          .then(response => {
            
            setOrders(response.data)
          })
          .catch(function (error) {
            console.log(error);
          });
      }

    } else {
      router.push('/payment/orders');
    }
  }
  const savedata = (e) => {
    MySwal.fire({
      title: 'กำลังบันทึก',
      showConfirmButton: false,
      onOpen: () => {
        MySwal.showLoading();
      }
    })
    e.preventDefault();
    var File = document.getElementById("img-premise").files;
    var formData = new FormData();
    formData.append('order_id', router.query.order_id);
    formData.append("file", File[0]);
    axios.post(`${process.env.host_api}/payment/up_status3`, formData, {
      headers: {
        'Content-Type': "multipart/form-data"
      }
    })
      .then(response => {
        console.log(response.data);
      MySwal.close();
        router.push('/payment/orders');
      })
      .catch(function (error) {
        console.log(error);
      });
  }
  const getFilename = (e) => {

    document.getElementById("label-filename").innerText = e.target.files[0].name;
  }
  return (
    <Layout>

      <div className="container cart-style" id='t'>
        <div className="card">
          <div className="card-body">
            {router.query.type_p == "1" ?
              <div className="table-responsive">
                <table className="table product-table">
                  <thead className="mdb-color lighten-5">
                    <tr>
                      <th style={{ width: "300px" }} />
                      <th className="font-weight-bold">
                        <strong>ชื่อสินค้า</strong>
                      </th>
                      <th />
                      <th className="font-weight-bold">
                        <strong>ราคา</strong>
                      </th>
                      <th className="font-weight-bold">
                        <strong>จำนวน</strong>
                      </th>
                      <th className="font-weight-bold">
                        <strong>ราคารวม</strong>
                      </th>
                    </tr>
                  </thead>
                  <tbody>
                    {orders.map((prod, i) => (
                      <tr key={i} >
                        <th scope="row">
                          <img src={prod.picture} className="img-fluid img-style z-depth-0" />
                        </th>
                        <td>
                          <h5 className="mt-3">
                            <strong>{prod.name}</strong>
                          </h5>
                        </td>
                        <td />
                        <td>
                          <a id="price">{prod.price}.00</a>
                        </td>
                        <td>
                          <strong>{prod.quantity_payment}</strong>
                        </td>
                        <td className="font-weight-bold">
                          <strong className="amount">{(prod.price * prod.quantity_payment).toLocaleString()}.00</strong>
                        </td>
                      </tr>
                    ))}
                    <tr>
                      <td colSpan={3} />
                      <td>
                        <h4 className="mt-2">
                          <strong>ราคาทั้งหมด</strong>
                        </h4>
                      </td>
                      <td colSpan={2} className="text-right">
                        <h4 className="mt-2">
                          <strong id="totalSum">{orders.reduce((a, b) => a + (b.price * b.quantity_payment), 0).toLocaleString()}.00</strong><strong > ฿</strong>
                        </h4>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
              :
              <div className="table-responsive">
                <table className="table product-table">
                  <thead className="mdb-color lighten-5">
                    <tr>
                      <th style={{ width: "300px" }} />
                      <th />
                      <th className="font-weight-bold">
                        <strong>ราคา</strong>
                      </th>
                      <th className="font-weight-bold">
                        <strong>ความกว้าง</strong>
                      </th>
                      <th className="font-weight-bold">
                        <strong>ความสูง</strong>
                      </th>
                      <th className="font-weight-bold">
                        <strong>จำนวน</strong>
                      </th>
                    </tr>
                  </thead>
                  <tbody>
                    {orders.map((prod, i) => (
                      <tr key={i} >
                        <th scope="row">
                          <img src={prod.design_picture} className="img-fluid img-style z-depth-0" />
                        </th>
                        <td />
                        <td>
                          <a id="price">{prod.production_price}.00</a>
                        </td>
                        <td>
                          <a >{prod.width}</a>
                        </td>
                        <td>
                          <a >{prod.height}</a>
                        </td>
                        <td>
                          <strong>{prod.quantity_payment}</strong>
                        </td>
                      </tr>
                    ))}
                    <tr>
                      <td colSpan={3} />
                      <td>
                        <h4 className="mt-2">
                          <strong>Total</strong>
                        </h4>
                      </td>
                      <td colSpan={2} className="text-right">
                        <h4 className="mt-2">
                          <strong >{orders.reduce((a, b) => a + (b.production_price * b.quantity_payment), 0).toLocaleString()}.00</strong><strong > ฿</strong>
                        </h4>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
            }
          </div>
        </div>
        <form onSubmit={((e) => savedata(e))}>
          <div className="card my-5">
            <div className="card-body">
              <div className="custom-file">
                <input type="file" className="custom-file-input" onChange={((e) => getFilename(e))} id="img-premise" required />
                <label className="custom-file-label" id="label-filename" >เลือกรูป</label>
              </div>
            </div>
            <div className="card-footer text-right">
              <button type="submit" className="btn btn-outline-success waves-effect" disabled={!orders.length ? "disabled" : ""}>บันทึก</button>
            </div>
          </div>
        </form>
        <div className="card my-5">
          <div className="card-body">
            <div className="row">
              {/* style={{height: "auto",width: "350px"}} */}
              <div className="col  " >
                <img src="../static/images/bbl.png" className="img-fluid" alt="Responsive image"></img>
              </div>
              <div className="col  ">
                <img src="../static/images/scb.png" className="img-fluid" alt="Responsive image"></img>
              </div>
              <div className="col ">
                <img src="../static/images/ktb.png" className="img-fluid" alt="Responsive image"></img>
              </div>
            </div>
          </div>
        </div>
      </div>
      <style jsx>{`
      .cart-style{
        margin-top: 10%;
      }
      .img-style {
          display: block;
          margin-left: auto;
          margin-right: auto;
          width: 80%;
      }
      
      `}</style>
    </Layout>
  );
}

export default payment_orders;
