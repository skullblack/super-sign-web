import React, { useEffect, useState } from 'react';
import Pagination from "react-js-pagination";

const test = () => {
  const [activePage, setActivePage] = useState(1);
  const handlePageChange = (pageNumber) => {
    console.log(`active page is ${pageNumber}`);
    setActivePage(pageNumber)
  }
  return (
    <div>
      <Pagination
        activePage={activePage}
        innerClass="pagination pg-blue"
        itemClass="page-item"
        linkClass="page-link"
        itemsCountPerPage={10}
        totalItemsCount={100}
        pageRangeDisplayed={10}
        onChange={handlePageChange}
      />
    </div>
  );
}
//tan

export default test;

