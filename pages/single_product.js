import React, { useState, useEffect } from 'react';
import { useRouter } from 'next/router';
import Layout from '../components/Layout';
import axios from 'axios';

const single_product = () => {
    const router = useRouter()
    const [orders, setOrders] = useState([]);
    const [countProduct, setCountproduct] = useState("");
    useEffect(() => {
        getstoSingle();
    }, [])
    const getstoSingle = () => {
        if (router.query.product_id) {
            axios.get(`${process.env.host_api}/storage/single/${router.query.product_id}`)
                .then(response => {
                    console.log(response.data);
                    
                    setOrders(response.data)
                })
                .catch(function (error) {
                    console.log(error);
                });
        } else {
            router.push('/products');
        }
    }

        const addCart = (e) => {
            
            var productid = e.target.productId.value;
            var productimg = e.target.productImg.value;
            var productname = e.target.productName.value;
            var productdetails = e.target.productDetails.value;
            var productprice = e.target.productPrice.value;
            e.preventDefault();
            var style = document.getElementById("button")
            $(style).slideUp(300).slideDown(300);
    
            if (!localStorage.getItem("cart")) {
                var carts = [];
                var setitem = {
                    id: productid,
                    img: productimg,
                    name: productname,
                    details: productdetails,
                    price: productprice,
                    count: 1
                };
                carts.push(setitem);
                localStorage.setItem("cart", JSON.stringify(carts));
            } else {
                var stoCart = JSON.parse(localStorage.getItem("cart"));
                var objIndex = stoCart.findIndex((obj => obj.id == productid));
                if (objIndex >= 0) {
    
                    stoCart[objIndex].count += 1;
                    localStorage.setItem("cart", JSON.stringify(stoCart));
                } else {
                    var setitem = {
                        id: productid,
                        img: productimg,
                        name: productname,
                        details: productdetails,
                        price: productprice,
                        count: 1
                    };
                    stoCart.push(setitem);
                    localStorage.setItem("cart", JSON.stringify(stoCart));
    
                }
            }
            var stoCartcheck = JSON.parse(localStorage.getItem("cart"));
            let sum = stoCartcheck.map(o => o.count).reduce((a, c) => { return a + c });
            setCountproduct(sum);
        }
    return (
        <Layout
        count={countProduct}
        >
            <div className=" container-fluid cart-style" >
            <div className="card cart-style my-5 mx-3 mx-md-5">
                <div className="card-header text-center">
                    ข้อมูลสินค้า
                </div>
                <div className="card-body ">
                <form onSubmit={((e) => addCart(e))}>
                    <section className="m-md-5 p-md-5">
                        {orders.length ? (orders.map((prod, i) => (
                            <div className="row" key={i}>
                                <input type="hidden" id="productId" value={prod.id} />
                                <input type="hidden" id="productImg" value={prod.picture} />
                                <input type="hidden" id="productName" value={prod.name} />
                                <input type="hidden" id="productDetails" value={prod.details} />
                                <input type="hidden" id="productPrice" value={prod.price} />
                                
                                <div className="col-lg-5">
                                    <div className="view overlay rounded z-depth-2 mb-lg-0 mb-4">
                                        <img className="img-fluid bg-black mx-auto" src={prod.picture} alt="Sample image" />
                                        <a>
                                            <div className="mask rgba-white-slight" />
                                        </a>
                                    </div>
                                </div>
                                <div className="col-lg-7">
                                    <h3 className="font-weight-bold mb-3"><strong>{prod.name}</strong></h3>
                                    <p>{prod.details}</p>
                                    <p><a><strong>ราคา</strong></a> : {prod.price}.00</p>
                                    <p><a><strong>วันที่จำหน่าย</strong></a> , {prod.date_product}</p>
                                    <div id="button" >
                                        <button type="submit"  className="btn btn-outline-danger waves-effect btn-md"><i className="fas fa-shopping-cart "> cart</i></button>
                                    </div>
                                </div>
                                
                            </div>
                        ))) :
                            <div className="text-center">
                                <div className="spinner-border text-primary " role="status" />
                            </div>
                        }
                    </section>
                    </form>
                </div>
            </div>
            </div>
            <style jsx>{`
      .cart-style{
        margin-top: 80px;
        margin-bottom: 25px;
      }
      .img-style {
          display: block;
          margin-left: auto;
          margin-right: auto;
          width: 80%;
      }
      
      `}</style>

        </Layout>
    );
}

export default single_product;
