import React, { useState, useEffect } from "react";
import Layout from '../components/Layout';
import Router from 'next/router';
import axios from 'axios';
import Swal from 'sweetalert2';
import withReactContent from 'sweetalert2-react-content';

const login_register = () => {
    const MySwal = withReactContent(Swal);
    const [dismiss, setDismiss] = useState(false);
    const [matchpass, setMatchpass] = useState(1);
    const [checklength, setChecklength] = useState(1);

    const loginSubmit = (e) => {
        e.preventDefault() // หยุดการทำงาน Submit
        const Username = e.target.username.value
        const Password = e.target.password.value
        setDismiss(!dismiss);
        console.log(Username + Password);

        axios.get(`${process.env.host_api}/user/check`, {
            // http://ss-api.inventage.co
            headers: {
                Username: Username, //Username
                Password: Password  //Password
            }
        })
            .then(function (response) {
                // setStorage
                console.log("tan");
                localStorage.setItem("Id", response.data.id);
                localStorage.setItem("Username", response.data.username);
                localStorage.setItem("Password", response.data.password);
                localStorage.setItem("Firstname", response.data.firstname);
                localStorage.setItem("Lastname", response.data.lastname);
                localStorage.setItem("Email", response.data.email);
                localStorage.setItem("Tel", response.data.tel);
                localStorage.setItem("Address", response.data.address);
                localStorage.setItem("Status", response.data.status);
                Router.push("/")
            })
            .catch(function (error) {
                setDismiss(dismiss);
                console.log(error);
                const Toast = MySwal.mixin({
                    toast: true,
                    position: 'top-end',
                    showConfirmButton: false,
                    timer: 4000,
                    timerProgressBar: true,
                    onOpen: (toast) => {
                        toast.addEventListener('mouseenter', MySwal.stopTimer)
                        toast.addEventListener('mouseleave', MySwal.resumeTimer)
                    }
                })

                Toast.fire({
                    icon: 'error',
                    title: 'รหัสผ่านผิด.'
                })
            });

    }

    const registerSubmit = (e) => {
        e.preventDefault()
        const Usernameregister = e.target.Usernameregister2.value
        const Passwordregister = e.target.registerpassword.value

        const firstnameregister = e.target.firstnameregister.value
        const lastnameregister = e.target.lastnameregister.value
        const emailregister = e.target.emailregister.value
        const telregister = e.target.telregister.value
        const addressregister = e.target.addressregister.value


        setDismiss(!dismiss);


        axios.put(`${process.env.host_api}/user`, {
            // http://ss-api.inventage.co

            username: Usernameregister,
            password: Passwordregister,
            firstname: firstnameregister,
            lastname: lastnameregister,
            email: emailregister,
            tel: telregister,
            address: addressregister

        })
            .then(function (response) {
                // console.log(response.data);
                document.getElementById("register").reset();
                const Toast = MySwal.mixin({
                    toast: true,
                    position: 'top-end',
                    showConfirmButton: false,
                    timer: 4000,
                    timerProgressBar: true,
                    onOpen: (toast) => {
                        toast.addEventListener('mouseenter', MySwal.stopTimer)
                        toast.addEventListener('mouseleave', MySwal.resumeTimer)
                    }
                })

                Toast.fire({
                    icon: 'success',
                    title: 'สมัครสำเร็จ'
                })
                setTimeout(function () {
                    setDismiss(dismiss);
                }, 1000);
            })
            .catch(function (error) {
                setDismiss(dismiss);
                console.log(error);
                if (error.response && error.response.status == 403) {
                    console.log(error.response.status);
                    const Toast = MySwal.mixin({
                        toast: true,
                        position: 'top-end',
                        showConfirmButton: false,
                        timer: 4000,
                        timerProgressBar: true,
                        onOpen: (toast) => {
                            toast.addEventListener('mouseenter', MySwal.stopTimer)
                            toast.addEventListener('mouseleave', MySwal.resumeTimer)
                        }
                    })

                    Toast.fire({
                        icon: 'error',
                        title: 'Username ถูกใช้งานแล้ว'
                    })
                }
            });
    }

    const matchPassword = () => {
        var Repeatpass = document.getElementById("registerconfirmpassword");
        var Passwordregister = document.getElementById("registerpassword");
        
        if (Passwordregister.value.length >= 10 ) {
            // console.log("ok");
            setChecklength(2);
            if (Repeatpass.value != Passwordregister.value) {
                setMatchpass(2)
            }else if (Repeatpass.value == Passwordregister.value) {
                setMatchpass(3);
            }else{
                setMatchpass(1)
            }
        } else if (Passwordregister.value.length == "" ) {
            setChecklength(1);
        } else {
            setChecklength(3);
            setMatchpass(1)
        }

        // if (Repeatpass.value == "" && Passwordregister.value == "") {
        //     setMatchpass(1)
        //     console.log(matchpass);
        // } else if (Repeatpass.value != Passwordregister.value) {
        //     setMatchpass(2)
        // } else {
        //     setMatchpass(3)
        // }

    }
    return (
        <Layout>
            <div style={{ marginTop: "130px" }} className="container-fluid">
                <div className="container my-3">
                    <h2 className="text-center mb-4" id="title">เข้าสู่ระบบและสมัครสมาชิก</h2>
                    {/* <p className="text-center">
                        <small id="passwordHelpInline" className="text-muted"> Developer: follow me on facebook <a href="https://www.facebook.com/JheanYu"> John niro yumang</a> inspired from <a href="https://p.w3layouts.com/">https://p.w3layouts.com/</a>.</small>
                    </p> */}
                    <hr />
                    <div className="row my-3">
                        <div className="col-md-5 order-3 order-lg-1">
                            <form role="form" id="register" method="post" onSubmit={(e) => registerSubmit(e)}>
                                <fieldset className="mb-5" disabled={dismiss ? "disabled" : ""}>
                                    <p className="text-uppercase pull-center"> สมัครสมาชิก :</p>
                                    <div className="form-group">
                                        <input type="text" name="Usernameregister2" id="Usernameregister2" className="form-control input-lg" placeholder="ชื่อผู้ใช้" required />
                                    </div>
                                    <div className="form-group">
                                        <input type="password" onChange={matchPassword} name="registerpassword" id="registerpassword" className="form-control input-lg" placeholder="รหัสผ่าน" required />
                                        {checklength != 2 && checklength != 1 ?
                                            <span className="badge badge-danger">ใส่รหัสอย่างน้อย 10 ตัวอักษร.</span>
                                            : ""}
                                    </div>
                                    <div className="form-group">
                                        <input type="password" onChange={matchPassword} name="registerconfirmpassword" id="registerconfirmpassword" className="form-control input-lg" placeholder="ยืนยันรหัสผ่าน" required />
                                        {matchpass != 1 && matchpass != 3  ?
                                            <span className="badge badge-danger">รหัสผ่านไม่ตรงกัน.</span>
                                            : ""}
                                    </div>
                                    <div className="form-group">
                                        <input type="text" name="firstnameregister" id="firstnameregister" className="form-control input-lg" placeholder="ชื่อ" required />
                                    </div>
                                    <div className="form-group">
                                        <input type="text" name="lastnameregister" id="lastnameregister" className="form-control input-lg" placeholder="สกุล" required />
                                    </div>
                                    <div className="form-group">
                                        <input type="text" name="emailregister" id="emailregister" className="form-control input-lg" placeholder="อีเมล์" required />
                                    </div>
                                    <div className="form-group">
                                        <input type="text" name="telregister" id="telregister" className="form-control input-lg" placeholder="เบอร์โทร" required />
                                    </div>
                                    <div className="form-group green-border-focus">
                                        <textarea className="form-control" name="addressregister" id="addressregister" placeholder="ที่อยู่" rows={3} defaultValue={""} required />
                                    </div>

                                    <div>
                                        <input type="submit" className="btn  btn-primary" value="สมัคร" disabled={matchpass != 3 ? "disabled" : ""} />
                                    </div>
                                </fieldset>
                            </form>
                        </div>
                        <div className="col-md-2 order-2 order-lg-2">
                        </div>
                        <div className="col-md-5 order-1 order-lg-3">
                            <form role="form" onSubmit={(e) => loginSubmit(e)}>
                                <fieldset className="mb-5" disabled={dismiss ? "disabled" : ""}>
                                    <p className="text-uppercase"> เข้าสู่ระบบโดยใช้บัญชีของคุณ : </p>
                                    <div className="form-group">
                                        <input type="text" name="username" id="username" className="form-control input-lg" placeholder="ชื่อผู้ใช้" required />
                                    </div>
                                    <div className="form-group">
                                        <input type="password" name="password" id="password" className="form-control input-lg" placeholder="รหัสผ่าน" required />
                                    </div>
                                    <div>
                                        <input type="submit" className="btn btn-md" value="เข้าสู่ระบบ" disabled={dismiss} />
                                    </div>
                                </fieldset>
                            </form>
                        </div>
                    </div>
                </div>
                {/* <p className="text-center">
                    <small id="passwordHelpInline" className="text-muted"> Developer:<a href="http://www.psau.edu.ph/"> Pampanga state agricultural university ?</a> BS. Information and technology students @2017 Credits: <a href="https://v4-alpha.getbootstrap.com/">boostrap v4.</a></small>
                </p> */}
            </div>

        </Layout>
    );
}

export default login_register;
